# Math Utilities Scripts
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
UNFINISHED UNRELEASED 
Public - Progress 30%
## Version
00.00.001.000
## Description

### Windows
    - CMD Batch or Powershell

    cd ./PSI_Scripts
    ./install.bat [uninstall.bat]
        or
    ./install.ps1 [uninstall.ps1]

        Follow instructions...

### Linux or Mac
    - Bash Script

    cd ./PSI_Scripts
    ./install.sh [uninstall.sh]

        Follow instructions...
