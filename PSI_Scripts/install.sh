#! /usr/bin/bash
# GET PATH

# MAKE DIR
mkdir /usr/bin/ulogik
mkdir /usr/lib/ulogik
mkdir /usr/include/ulogik
# COPY FILES
cp  -R ../uLTRALogik/bin/* /usr/bin/ulogik
cp  -R ../Math-Utilities/_Library/*  /usr/lib/ulogik
cp  -R ../Math-Utilities/*  /usr/include/ulogik
# RUN TESTS