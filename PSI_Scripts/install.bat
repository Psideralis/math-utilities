:: GET PATH

:: CREATE DIR
@mkdir PsideralisMathUtilities
@mkdir PsideralisMathUtilities/bin
@mkdir PsideralisMathUtilities/lib
@mkdir PsideralisMathUtilities/inc
:: COPY FILES
@copy ../uLTRALogik/bin/* /PsideralisMathUtilities/bin
@copy ../Math-Utilities/_Library/* /PsideralisMathUtilities/lib
@copy ../Math-Utilities/* /PsideralisMathUtilities/inc
:: RUN TEST
@echo 