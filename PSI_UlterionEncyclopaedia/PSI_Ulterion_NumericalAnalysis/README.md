# Ulterion Numerical Analysis Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
   Analysis = {Change,Mode}
********************************************************************************
    Derivatives
        -Ordinaries
        -Partial
        -Vectorial
        -Tensorial
    Integrals
        -Scalar
        -Vectorial
        -Tensorial
    Analytical Algebra
        -Differential
            -Scalar
            -Vectorial
            -Tensorial
        -Variational
            -Scalar
            -Vectorial
            -Tensorial
    Differential equations
        -Ordinary
        -Partial
    Variational equations
        -Sup/Inf
        -Min/Max
********************************************************************************