# Ulterion Arithmetics Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
    Numeric System = {Value,Quantity}
********************************************************************************
    Numeric domains (Natural, Integer, Rational, Real, Complex)
    Numeric systems: Arithmetic Algebras
        -Binary arithmetics 
        -Module arithmetics
            -Octal              8
            -Hexadecimal        16
            -Ditrigesimal       32
            -Tetrahexagesimal   64
        -Real arithmetics
        -Complex arithmetics
        -Conversion arithmetics
    Big number arithmetics
            128 bit: 16 bytes
            256 bit: 32 bytes
            512 bit: 64 bytes
            1024 bit: 128 bytes
            2048 bit: 256 bytes
            4096 bit: 512 bytes
    Numeric series
        -Polynomic
        -Radical
        -Rational
        -Trascendental
        -Differential
        -Integral
        -Variational
    Numeric composition
        -Prime sum compositions (Sum product composition: powered, rooted)
        -Product sum composition (powered, rooted)
********************************************************************************