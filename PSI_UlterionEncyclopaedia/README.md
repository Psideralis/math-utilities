# Ulterion Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
Octarium Codegrammer uLTRALogic samples. Psideralis Commercial subscription includes audio, video, Q&A voice and text chat bot, learning path, badges and certifications. Psideralis elite subcription includes titles and recruitment and job search engines.

# Languages
   - Spanish
   - English
   - French
   - German
   - Italian
   - Portuguese
   - Russian
   - Dutch
   - Hebrew
   - Greek
   - Latin
   - Arabic
   - Persian
   - Ge'ez
   - Amharic
   - Hindi
   - Sanskrit
   - Chinese
   - Japanese
   - Korean
   - Tibetan

# Logic {Symbols,Rules}
   Logic Devices
   Sets
   Combinatorics
   Graphs
   Models
   Problems
   Resolutions
   Expert Systems 

# Arithmetics {Value,Quantity}
   Numeric domains
   Numeric systems
   Big number arithmetics
   Numeric series
   Numeric composition

# Geometry {Vertex,Topes}
   Coordinates
   NonStandard geometry
   Geometrical algebra
   Geometrical analysis
   Geometrical combinatorics

# Algebra {Set,Operators}
   Symbolic systems
   Abstract algebra
   Computational agebra
   Universal algebra
   Categories

# Numerical Analysis {Change,Mode}
   Derivatives
   Integrals
   Analytical algebra
   Differential equations
   Variational equations

# Topology {Points,Morfors}
   General
   Algebraic
   Geometric
   Analytical

# Statistics {Descriptor,Measure}
   Descriptive statistics
   Inferential statistics
   Hypothesis falsification
   Probability

# Optimization {Contrains,Ranges} 
   Algebraic    
   Geometrical
   Combinatorial
   Analytical

# Machine Learning {Adaptation, Mutation}
   Analytic Statistics: Semisupervised Learning
   Predictive Statistics: Supervised Learning
   Optimization Statistics: Reinforced Learning
   Metamodelling Statistics: Unsupersived Learning

# Systems {Signals, Input, Output, Pertubation} & Control {Sensors, Actuators, Processing Unit}
   Signals
   Frequency & Time Domain
   Circuit System Methods
   State Space Methods
   State Machine Methods