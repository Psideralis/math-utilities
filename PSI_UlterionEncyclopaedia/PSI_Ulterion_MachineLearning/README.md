# Ulterion Machine Learning Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
   Learning = {Adaptation,Modification}
********************************************************************************
   Analytic Statistics (Semisupervised Learning) :
            - Dimensionality Reduction/Aumentation
            - Support Vector Machines (Segmentation)
            - Correlating (Classification)
            - Aggrupation (Clustering)
   Predictive Statistics (Semisupervised Learning):
            - Association Rules
            - Decision Trees
            - Markov Chains
            - Bayesian Networks (Included under Statistics: probability.hpp)
            - Regression Models
   Optimization Statistics (Reinforced Learning):
            - Neural Networks
            - Genetic & Evolutionary Algorithms
   Metamodelling Statistics (Unsupervised Learning): 
            - Axionic Algorithms
********************************************************************************