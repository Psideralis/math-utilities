# Ulterion Logic Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
   Logic = {Symbols,Rules}
********************************************************************************
    Logic Devices: Only propositional and predicative are included is Psideralis Math Utilities. Contact Psideralis for full commercial version.
        STRUCTURE:
            - Set of symbols
            - Ordination rules      (Ordenance)
            - Formation rules       (Morphism)
            - Transformation rules  (Inference)
            - Axioms
            - Theorems
            - Coloraries
            - Lemmas
            - Scholiums
            - Demostration
            - Proofs
            - Conjectures
            - Hypothesis
        TYPES:
            - Propositional Logic           (Truth Tables: 2^n)[Included]                  
                Ex: (Not P) or Q, "The house is not yellow or the tree is green." 
            - Predicative Logic             (Truth Tables: Finite Set Universe: 2^n*|x|)[Included]    
                Ex: (x)(Px then Qx), "For all x if x is a dog then x is an animal."
---
            - Relational Logic              (Truth Tables: Finite Set Universe: 2^n*|x|*|y|)[Not Included]   
                Ex: (x)(Ey)(Px then Qxy), "For all x there is at least one y so that when x is a tree then y is a fruit of x."
            - Operative Logic               (Truth Tables: Finite Set Universe: 2^n*|x|*|y*|z|)    
                Ex: (x)(Ey)(Ez)(Px then xQyz), "For all x there at least one y and one z that when x is a postofficer then x delivers y in z."
            - Temporal Logic                (Truth Tables: Finite Set Universe: 2^n*|t|) 
                Ex: (x,Et[:])(Px or Qx), "For all x there is at least one time instant so that x is static or x is moving."
            - Spatial Logic:                (Truth Tables: Finite Set Universe: 2^n*|s|) 
                Ex  (x,Es[:])(Px or Qx), "For all x there is at least one space point so that x is present or x is auscent."
            - Mechanical or Eventual Logic: (Truth Tables: Finite Set Universe: 2^n*|s|*|t|) 
                Ex: (Ex,Et[0:t],Es[0:s])(Px or Qx), "The is at least one x, at least one time interval and at least one space region where x is traslating or x is rotating."
            - Modal Logic                   (Truth Tables: Finite Set Universe: 2^n) 
                Ex: (Ex)(□Px then ◇Qx), "There is at least one x so that if is possible that x is a thread then is necessary that x is prevented."
            - Probabilistic Logic           (Thruth Tables: 2^n*|x| + Probability Distribution: Descriptive, Inferential or Prescriptive)
                Ex: (Δ:x)(Qx or Sx), "There is a n probability that for all x, x is a dog or x is a cat"
            - Subjective Logic              (Thruth Tables: 2^n*|S|*|x|)
                Ex: (ES)((x)(Px or Qx), "There is at least one subject for whom for all x, x is a dog or x is a cat."
            - Organizational Logic          (Thruth Tables: 2^n*|o|*|x|)
                Ex: (Eo)((x)Px or Qx), "There is at least one organization for who for all x, x is a enemy or x is an allied."
            - Interpretative Logic          (Thruth Tables: 2^n*|S|*|O|*|x|)
                Ex: (ES,O)((x)Px or Qx), "There is at least one subject for whom based on O, for all x, x is a dog or x is a cat.
            - Interrogative Logic           (Thruth Tables: 2^n*|x|)
                Ex: (E?x)(Px and Qx),"Is there an x which is a door and is red?"
            - Analytic Logic                (Thruth Tables: 2^n*|x|)
                Ex: P[(Ex)(Px and Qx)],"Is there at least one x which is a door and is red?"
            - Predictive Logic              (Thruth Tables: 2^n*|x|)
                Ex: (Ex)(P?x and Q?x),"Is there at least one x which is a door and is red?"
            - Prescriptive Logic            (Thruth Tables: 2^n*|x|)
                Ex: (x)(Px then Q>x), "For all x if x is an effective cure then x must operate on the active site of the pathogen."
            - Decisive Logic                (Thruth Tables: 2^n*|R|*|x[:]|)
                Ex: S,o(01,02,...,0N)(x)(P<x then Q>x), "Given a ordination R for all x if x should we feasible then x must be done."
            - Axiomatic Logic               (Thruth Tables: 2^n*|R|*|x[:]|)
                Ex: S,o(O1,O2,...,ON)(x)(Ey)(Px then Qxy), "Given an axiologic ordination for all x there is an y so that if x in right then x is permited." 
            - Imperative Logic              (Thruth Tables: 2^n*|S,o(R)|*|x[:]|*|y[:]|)
                Ex: S,o(I1,I2,...,IN)(x)(Ey)(Px then Qxy),"Given an imperative ordination for all x there is an y so that if x is mandatory then y is enable."
            - Affective Logic               (Thruth Tables: 2^n*|S,o(R)|*|S,o(x[:])|*|S,o(y[:])|)
                Ex: S,o(F1,F2,...,FN)(x)(Ey)(Pxy then Qxy), "Given an affection ordination for all x there is an y so that if x is more affective than y then x is preferable than y."
            - Desiderative Logic            (Thruth Tables: 2^n*|S,o(R)|*|S,o(x[:])|*|S,o(y[:])|)
                Ex: S,o(D1,D2,...,DN)(x)(Ey)(Px then Qxy),"Given an desiderative ordination for all x there is an y so that when x is enable then y must follow x."       
            - Inductive Logic               (Inductive Proof)
                Ex: P(0,1,2,3...) is truth. Hypothesis: If P(N) is truth then P(N+1) is truth.
            - Abductive Logic               (Incompletness Information Theorem: Incomplete Thruth Tables entries)
                Ex: (75%:I)(Px or Qx), "For the current known information is feasible that x is a dog or x is a cat."
            
        OPERATORS:
            - Thruth value
            - Theorems completition & discovery
            - Demostration & proofs
            - Conjectures & hypothesis
            - Logic function modelling
            - Knowledge discovery & representation
    Sets
        - Sets: Natural, Integer, Rational, Real and Complex
        - Ordered sets
        - Power sets
        - Boolean sets
        - Space series
        - Time series
        - Event series
        - Point series
        - Functions
        - Functional
    Combinatorics
        - Combinations
        - Ordered lists
        - Permutations
    Graphs:
        - Trees
        - Graphs
        - Graphor
    Models
        - Scalar
        - Vector
        - Tensor
        - Differential
        - Variational
        - Constant coefficients
        - Variable coefficients
        - Unconstrained
        - Constrained
    Problems
        - Procedural            
        - Functional
        - Methodical
        - Conditional
        - Iterative 
        - Recursive
        - Transactional
        - Eventual 
        - Probabilistic
    Resolutions
        - Heuristic
        - Systematic
        - Casuistic
        - Casualistic
        - Creative
        - Organic
        - Mechanic
        - Instrumental
    Expert Systems:
        - Religious (Theological, Mythological)
        - Philosophical & Philological (Literary)
        - Formal & Material  (Natural, Positive)
        - Biotic & Antropic* (Individual, Social)
        - Applied (Medical, Educational, Sportive)
        - Engineerial & Technological
        - Art & Cultural
        - Recreational & Ludical
********************************************************************************