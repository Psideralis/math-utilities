# Ulterion Algebra Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
   Algebra System = {Set,Operators}
********************************************************************************
   Symbolic systems
       Linear & Multilinear algebra
       Scalar & Vector & Matrix & Tensor algebra
       Equations & Inequations
       Constant & Variable Coefficient
       Multivariable
       Constrained & Unconstrained algebra
       Differential & Variational algebra
       NonLinear & NonMultilinear algebra  (Polynomial, Radical, Rational, Trascendental, Special)
   Abstract algebra
       Set (Natural, Integer, Rational, Real, Complex)
       Unary structures:
          Magma
          Semigroup  
          Monoid  
          Group   
       Binary structures:
          Conmutative Group
          Ring
          Unitary Ring
          Body
       N-ary structures:
          Module
          Reticule
          Vector Space
          Tensor Space
   Computational Algebra
       Automata
       Finite State Machines
       Lambda Calculus
   Universal algebra
       Arithmetic Algebra
       Geometric Algebra
       Analytic Algebra
       Synthetic Algebra
       Statistical Algebra
   Categories
       Functors
       Lattices
       Isomorphisms
********************************************************************************