# Ulterion Geometry Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
   Form = {Vertex,Topes}
********************************************************************************
    Coordinates
        -Cartesian
        -Sphere
        -Cylindrical
        -Angular
        -Harmonic
        -Prophonic
        -Polar
    Topes 
        -Points
        -Lines, Curves
        -Planes, Surfaces
        -Volumes, Solids
        -Hypersolids
    Vertex  
        -Angles
        -Arcangles
        -SolidAngles
        -SolidArcangles
        -HyperAngles
    Polytopes 
        -Regular 
        -Irregular
        -Disruptive
        -Polygon, Polyhedron, Hyperpolytopes Charts
    Manifolds 
        -Smooth
        -Irregular
        -Disruptive
        -Manifold charts
    (Non/Post)Reimannian Phase & Geometry (Nonstandard Metric)
        -Normal Forms
        -Harmonic Forms
        -Polar Forms
        -Prophonic Forms
        -Irregular Forms
        -Disruptive Forms
        -Phase charts
    Geometrical algebra
        - Static
            -Magnitudinal
        - Kinematic
            -Traslational
            -Rotational
            -Deformational
            -Defractional
            -Deplosional
        - Dynamic
            - Potential
            - Conmmanental
    Geometrical analysis
        - Time dependant
        - Space dependant
        - Time-Space dependent
        - Probabilistic depentant
        - Statistical dependant
    Geometrical combinatorics
        - Synthetic geometry
********************************************************************************