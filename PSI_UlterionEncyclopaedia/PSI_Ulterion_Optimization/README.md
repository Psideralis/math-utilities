# Ulterion Optimization Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
   Optimization = {Contrains,Operacma}
********************************************************************************
    Algebraic    
        -Linear
        -Nonlinear
        -Multilinear
        -Nonmulilinear
    Geometrical
        -Topic
        -Manifold
    Combinatorial
        -Combination
        -Order
        -Permutation
    Analytical
        -Differential
        -Variational
********************************************************************************