# Ulterion Topology Encyclopediae
Psideralis
## License
Psideralis Private Commune License
## Status
UNFINISHED
## Version
00.00.000.001
## Description
********************************************************************************
    Topology = {Points,Morfors}
********************************************************************************
    General
    Algebraic
        -Linear
        -Nonlinear
        -Multilear  
        -Nonmultilinear
    Geometric
        -Topes
        -Manifolds
    Analytical
        -Differential
        -Variational
********************************************************************************