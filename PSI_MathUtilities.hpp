/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name:  PsideralisMathUtilities.hpp

See a specific content for further instructions.
    - Logic
    - Arithmetics
    - Geometry
    - Algebra    
    - Topology
    - Numerical Analysis
    - Statistics
    - Optimization
    - Control
    - MachineLearning
********************************************* */ 

#ifndef PSI_MATHUTILITIES
#define PSI_MATHUTILITIES

#include "PSI_MathUtilities/_Assembly/PSI_MathAssembly.hpp"
#include "PSI_MathUtilities/_Include/PSI_Logic/PSI_LogicToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Algebra/PSI_AlgebraToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Arithmetics/PSI_ArithmeticsToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Geometry/PSI_GeometryToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_NumericalAnalysis/PSI_NumericalAnalysisToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Optimization/PSI_OptimizationToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Topology/PSI_TopologyToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Control/PSI_ControlToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_MachineLearning/PSI_MachineLearningToolbox.hpp"
#include "PSI_MathUtilities/_Include/PSI_Statistics/PSI_StatisticsToolbox.hpp"

using namespace PSI_MathUtility;
using namespace PSI_MathUtility::PSI_LogicToolbox;
using namespace PSI_MathUtility::PSI_AlgebraToolbox;
using namespace PSI_MathUtility::PSI_ArithmeticToolbox;
using namespace PSI_MathUtility::PSI_GeometryToolbox;
using namespace PSI_MathUtility::PSI_NumericalAnalysisToolbox;
using namespace PSI_MathUtility::PSI_OptimizationToolbox;
using namespace PSI_MathUtility::PSI_TopologyToolbox;
using namespace PSI_MathUtility::PSI_ControlToolbox;
using namespace PSI_MathUtility::PSI_StatisticsToolbox;
using namespace PSI_MathUtility::PSI_MachLearningToolbox;

#endif