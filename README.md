# Math Utilities
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
UNFINISHED UNRELEASED
Public - Progress 30%
## Version
00.00.001.000

## Description
This math ASM, C, C++, C#, Python and Javascript library encompasses logic, arithmetics, algebra, geometry, topology, numerical analysis, statistical, optimization, control and machine learning as part of computational logic and computational mathematics, bases of uLTRALogic Studio, a studio for logical and mathematical development and analysis. We will offer a Free Software mayor command line version of uLTRALogic, separate visualization tools and a Octarium Codegrammer version with Ulterion Encyclopaedia. For a privative commercial Psideralis full mayor GUI, HPC, Cloud & Server, VR and Mobile versions or the Python and Javascript modules contact us for more information. You can install the headers as a developmental static library for specific code requirements, or use the dynamic library linked for your own applications for static and dynamic linking, or use the command line and visualization tools, on any platform (Windows, Linux, MacOS).

The CLI connects to a remote repository of community & individual free software and commercial privative libraries. New repositories can be created as free software or commercial privative repositorias via the CLI. Commercial private rights requires Psideralis uLTRALogic web site registration and includes terms & conditions and a fee. 

VISITED THE WIKI FOR MORE INFORMATION ABOUT INSTALLATION, USE AND DERIVATES. A ZIP RELEASE IS DATED FOR THE END OF 2030 INCLUDING STATISTICS, LOGIC AND ALGEBRA. Wiki: https://gitlab.com/Psideralis/math-utilities/-/wikis/home

# Content
    - Logic
    - Arithmetics
    - Geometry
    - Algebra    
    - Topology
    - Numerical Analysis
    - Statistics
    - Optimization
    - Control
    - Machine Learning
    - Visualization Tool: uLTRAplot CLI
    - uLTRALogic Studio CLI
    - uLTRALogic Octarium Codegrammer
    - Hyperion Encyclopaedia


# Development header

    #include "PSI_MathUtilities.hpp"
    using namespace PSI_MathUtility;

    or 

    #include "PSI_MathUtilities/_Include/PSI_Algebra/PSI_AlgebraToolbox.hpp"
    using namespace PSI_MathUtility::PSI_AlgebraToolbox;

    or

    #include "PSI_MathUtilities/_Include/PSI_Algebra/PSI_UniversalAlgebraModule.hpp"
    using namespace PSI_MathUtility::PSI_AlgebraToolbox::PSI_UniversalAlgebraModule;

    int main(int argc, char* argv[]){
        PSI_Init(rgc, argv);
        /*

            CODE

        */
        PSI_Free();
        return 0;
    }



# Static Link compilation
    Windows:
        cl /I [PSI_MathUtilities.hpp path] or use [PSI_MathUtilities.lib]
    Linux:
        g++ -I [PSI_MathUtilities.hpp path] or use [PSI_MathUtilities.so]
    Mac:
        clang++ -i [PSI_MathUtilities.hpp path] or use [PSI_MathUtilities.so]
        
# Dynamic Link compilation
    Windows:
        cl /link [PSI_MathUtilities.dll path]
    Linux:
        g++ -L [PSI_MathUtilities.a path]
    Mac:
        clang++ -l [PSI_MathUtilities.dynlib path]

# C# Dynamic Linking

    dotnet install PSI_MathUtilities

    For C# wrapper of complete framwork:

    using PSI_MathUtilities;

    or for specifics:

    using PSI_MathUtilities.PSI_LogicToolkit;
    using PSI_MathUtilities.PSI_ArithmeticToolkit;
    using PSI_MathUtilities.PSI_GeometryToolkit;
    using PSI_MathUtilities.PSI_TopologyToolkit;
    using PSI_MathUtilities.PSI_NumericalAnalysisToolkit;
    using PSI_MathUtilities.PSI_StatisticToolkit;
    using PSI_MathUtilities.PSI_OptimizationToolkit;
    using PSI_MathUtilities.PSI_MachineLearningToolkit;
    using PSI_MathUtilities.PSI_ControlToolkit;

    ⚠️ Don't forget to include for your compiler the dll's path.
        - PSI_SharpMathUtilities.dll

    ⚠️ Don't forget to tell to the compiler to use of unsafe code:
    Compiler option: /unsafe
        or
    <PropertyGroup>
        ...
        <AllowUnsafeBlocks>true</AllowUnsafeBlocks>
        ...
    </PropertyGroup>

# Javascript

    const PSI_MathUtilities = require(PSI_MathUtilities);

    Contact Psideralis for more information.

# Python

    import PSI_MathUtilities

    Contact Psideralis for more information.

# Alternatives
## Library Alternatives
    C++:
        - Intel oneAPI Math Kernel Library
        - GNU Scientific Library
        - GNU Multiple Precision Arithmetic Library
        - IML++
        - IT++
        - NTL
    C#:
        - ILNumerics
        - Math.NET Numerics
    Javascript:
        - NumericJS
    Python:
        - Numpy
        - Scipy

## GUI Alternatives
    - Matlab
    - Mathematica
    - Scilab
    - GNU Octave
    - OpenModelica
    - SageMath
    - Maple
    - SPSS
    - Excel
