# uLTRALogik CLI
## Author: 
Psideralis
## License: 
Psideralis Commune Public License
## Status:
Public - Progress: 0%
## Version
00.00.000.001
## Description:

## Manual:

    [global options] <subcommands> [options] <files>
    
    parameters list:
        -d, -dir: current directory
        -c, -cmd:   command line mode
        -h, -help : display help
        -s, -script "path" : run script
        -u, -update : search for updates repository database
        -d, -upgrade : search for upgrade repository database
        -n, -repository "repository name": create and name repository
        -p, -publish "repository name": publish repository
        -a, -add "module name" "repository name": add module to repository
        -g, -get "module name" "repository name": get module from repository
        -r, -remove "module name" "repository name": remove module from repository
        -t, -update "module name" "repository name": update module from repository
        -dba, -dbadd "repository database name": add repository database
        -dbd, -dbdelete "repository database ": delete repository database

    options list:

    module format:
        -[module name].zip || [module name].tar
            -manifest.ux
            -license
            -dynamic libraries
            -static libraries
            -headers
            -source
