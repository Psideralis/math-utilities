# uLTRAplot
## Author: 
Psideralis
## License: 
Psideralis Commune Public License
## Status:
Public - Progress: 0%
## Version
00.00.000.001
## Description:


## Manual: 

    uplot [global options] <subcommands> [options]  <files>

    parameters list:
        -n, -name   [name]:      output file name
        -f, -format [format]:    format raw data file 
        -i, -input  [format]:    input format format
        -o, -output [format]:    output format file
        -c, -cdir [path]:        set current directory
        -d, -odir [path]:        set output directory    
        -i, -interpreter:        interpreter mode

    option list:
        -2d:  2D Plotter of points, curve, surfaces(gradient), volumes(digrapdient); time dependant, statistical
        -3d:  3D Plotter of points, curves, surfaces or volumes; time dependant, statistical
        -sfield:  Scalar Field; time dependant, statistical
        -vfield:  Vector Field; time dependant, statistical
        -tfield:  Tensor Field; time dependant, statistical
        -fdiam:  Fasorial Diagramer
        -bdiam:  Bode Diagramer
        -pdiam:  Polar Diagramer
        -ndiam:  Nyquist Diagramer
        -schart:  Smith Chart
        -pgraph:  Pantograph Diagramer
        -pmorphic:  Paramorphic Diagramer: pluraritmic, retrocursive; time dependant, statistical
        -dblock:  Blocks Diagramer