/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PsideralisMathAssembly.hpp
Description: Assembly Math Utilities
********************************************* */ 

/* *********************************************
DEFINES:
    ASSEMBLY
********************************************* */ 

#ifndef ASSEMBLY
#define ASSEMBLY

 
/* ******************************************************

                            LOGIC

****************************************************** */
/*

    AND

*/
extern "C" int asm_gnu_and_16();
extern "C" int asm_gnu_and_32();
extern "C" int asm_gnu_and_64();
extern "C" int asm_gnu_and_128();
extern "C" int asm_gnu_and_256();
extern "C" int asm_gnu_and_512();
extern "C" int asm_gnu_and_1024();
extern "C" int asm_gnu_and_2048();
extern "C" int asm_gnu_and_5096();
extern "C" int asm_gnu_and_10192();
extern "C" int asm_gnu_and_20384();
extern "C" int asm_nasm_and_16();
extern "C" int asm_nasm_and_32();
extern "C" int asm_nasm_and_64();
extern "C" int asm_nasm_and_128();
extern "C" int asm_nasm_and_256();
extern "C" int asm_nasm_and_512();
extern "C" int asm_nasm_and_1024();
extern "C" int asm_nasm_and_2048();
extern "C" int asm_nasm_and_5096();
extern "C" int asm_nasm_and_10192();
extern "C" int asm_nasm_and_20384();
extern "C" int asm_masm_and_16();
extern "C" int asm_masm_and_32();
extern "C" int asm_masm_and_64();
extern "C" int asm_masm_and_128();
extern "C" int asm_masm_and_256();
extern "C" int asm_masm_and_512();
extern "C" int asm_masm_and_1024();
extern "C" int asm_masm_and_2048();
extern "C" int asm_masm_and_5096();
extern "C" int asm_masm_and_10192();
extern "C" int asm_masm_and_20384();
/* ******************************************************

                            BINARY

****************************************************** */
/*

    RIGHT SHIFT

*/
extern "C" int asm_gnu_rshift_16();
extern "C" int asm_gnu_rshift_32();
extern "C" int asm_gnu_rshift_64();
extern "C" int asm_gnu_rshift_128();
extern "C" int asm_gnu_rshift_256();
extern "C" int asm_gnu_rshift_512();
extern "C" int asm_nasm_rshift_16();
extern "C" int asm_nasm_rshift_32();
extern "C" int asm_nasm_rshift_64();
extern "C" int asm_nasm_rshift_128();
extern "C" int asm_nasm_rshift_256();
extern "C" int asm_nasm_rshift_512();
extern "C" int asm_masm_rshift_16();
extern "C" int asm_masm_rshift_32();
extern "C" int asm_masm_rshift_64();
extern "C" int asm_masm_rshift_128();
extern "C" int asm_masm_rshift_256();
extern "C" int asm_masm_rshift_512();

/* ******************************************************

                        ARITHMETICS

****************************************************** */
/*

    ADD

*/
extern "C" int asm_gnu_add_16();
extern "C" int asm_gnu_add_32();
extern "C" int asm_gnu_add_64();
extern "C" int asm_gnu_add_128();
extern "C" int asm_gnu_add_256();
extern "C" int asm_gnu_add_512();
extern "C" int asm_nasm_add_16();
extern "C" int asm_nasm_add_32();
extern "C" int asm_nasm_add_64();
extern "C" int asm_nasm_add_128();
extern "C" int asm_nasm_add_256();
extern "C" int asm_nasm_add_512();
extern "C" int asm_masm_add_16();
extern "C" int asm_masm_add_32();
extern "C" int asm_masm_add_64();
extern "C" int asm_masm_add_128();
extern "C" int asm_masm_add_256();
extern "C" int asm_masm_add_512();
/*

    SUBSTRACT

*/
extern "C" int asm_gnu_res_16();
extern "C" int asm_gnu_res_32();
extern "C" int asm_gnu_res_64();
extern "C" int asm_gnu_res_128();
extern "C" int asm_gnu_res_256();
extern "C" int asm_gnu_res_512();
extern "C" int asm_nasm_res_16();
extern "C" int asm_nasm_res_32();
extern "C" int asm_nasm_res_64();
extern "C" int asm_nasm_res_128();
extern "C" int asm_nasm_res_256();
extern "C" int asm_nasm_res_512();
extern "C" int asm_masm_res_16();
extern "C" int asm_masm_res_32();
extern "C" int asm_masm_res_64();
extern "C" int asm_masm_res_128();
extern "C" int asm_masm_res_256();
extern "C" int asm_masm_res_512();
/*

    MULTILICATE

*/
extern "C" int asm_gnu_mul_16();
extern "C" int asm_gnu_mul_32();
extern "C" int asm_gnu_mul_64();
extern "C" int asm_gnu_mul_128();
extern "C" int asm_gnu_mul_256();
extern "C" int asm_gnu_mul_512();
extern "C" int asm_nasm_mul_16();
extern "C" int asm_nasm_mul_32();
extern "C" int asm_nasm_mul_64();
extern "C" int asm_nasm_mul_128();
extern "C" int asm_nasm_mul_256();
extern "C" int asm_nasm_mul_512();
extern "C" int asm_masm_mul_16();
extern "C" int asm_masm_mul_32();
extern "C" int asm_masm_mul_64();
extern "C" int asm_masm_mul_128();
extern "C" int asm_masm_mul_256();
extern "C" int asm_masm_mul_512();
/*

    DIVIDE

*/
extern "C" int asm_gnu_div_16();
extern "C" int asm_gnu_div_32();
extern "C" int asm_gnu_div_64();
extern "C" int asm_gnu_div_128();
extern "C" int asm_gnu_div_256();
extern "C" int asm_gnu_div_512();
extern "C" int asm_nasm_div_16();
extern "C" int asm_nasm_div_32();
extern "C" int asm_nasm_div_64();
extern "C" int asm_nasm_div_128();
extern "C" int asm_nasm_div_256();
extern "C" int asm_nasm_div_512();
extern "C" int asm_masm_div_16();
extern "C" int asm_masm_div_32();
extern "C" int asm_masm_div_64();
extern "C" int asm_masm_div_128();
extern "C" int asm_masm_div_256();
extern "C" int asm_masm_div_512();
/*

    MODE

*/
extern "C" int asm_gnu_mod_16();
extern "C" int asm_gnu_mod_32();
extern "C" int asm_gnu_mod_64();
extern "C" int asm_gnu_mod_128();
extern "C" int asm_gnu_mod_256();
extern "C" int asm_gnu_mod_512();
extern "C" int asm_nasm_mod_16();
extern "C" int asm_nasm_mod_32();
extern "C" int asm_nasm_mod_64();
extern "C" int asm_nasm_mod_128();
extern "C" int asm_nasm_mod_256();
extern "C" int asm_nasm_mod_512();
extern "C" int asm_masm_mod_16();
extern "C" int asm_masm_mod_32();
extern "C" int asm_masm_mod_64();
extern "C" int asm_masm_mod_128();
extern "C" int asm_masm_mod_256();
extern "C" int asm_masm_mod_512();


/* ******************************************************

                        ALGEBRAIC

****************************************************** */
/*

    EQUAL

*/
extern "C" int asm_gnu_eq_16();
extern "C" int asm_gnu_eq_32();
extern "C" int asm_gnu_eq_64();
extern "C" int asm_gnu_eq_128();
extern "C" int asm_gnu_eq_256();
extern "C" int asm_gnu_eq_512();
extern "C" int asm_nasm_eq_16();
extern "C" int asm_nasm_eq_32();
extern "C" int asm_nasm_eq_64();
extern "C" int asm_nasm_eq_128();
extern "C" int asm_nasm_eq_256();
extern "C" int asm_nasm_eq_512();
extern "C" int asm_masm_eq_16();
extern "C" int asm_masm_eq_32();
extern "C" int asm_masm_eq_64();
extern "C" int asm_masm_eq_128();
extern "C" int asm_masm_eq_256();
extern "C" int asm_masm_eq_512();

#endif