# Assembly Bibliothek
## Author
Psideralis
## License
GNU General Public License 3.0 & Psideralis Commune Public License
## Version
00.00.000.005
## Status
Public - Progress 25%
## Compiler:
    - GNU AS
    - NASM
    - MASM
## Operations:
    - Add
    - Substract
    - Multiply
    - Divide
    - Module
