# Psideralis Math Utilities: Machine Learning
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public - Progress 0%
## Version
00.00.000.001
## Description
Machine learning toolbox library for:

        Analytic Statistics (Semisupervised Learning) :
                Dimensionality Reduction/Aumentation
                Support Vector Machines (Segmentation)
                Correlating (Classification)
                Aggrupation (Clustering)
        Predictive Statistics (Semisupervised Learning):
                Association Rules
                Decision Trees
                Markov Chains
                Bayesian Networks (Included under Statistics: probability.hpp)
                Regression Models
        Optimization Statistics (Reinforced Learning):
                Neural Networks
                Genetic & Evolutionary Algorithms
        Metamodelling Statistics (Unsupervised Learning): 
                Axionic Algorithms

# Artificial Intelligence

        Natural Language Processing:
                Symbolic Recognition
                Symbolic Synthesis
        Computer Vision:
                Object Recognition
                Kinematic Recognition
                Writing Recognition
        Computer Audition:
                Speech Recognition        
        Computer Locution:
                Speech Synthesis
        Robotics:
                Kinematic Synthesis
                Writing Synthesis