/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_MachLearningToolbox.hpp
Description: A machine learning toolbox library.
//............................................//
For:
	Statistics:
		- See PSI_StatisticsToolbox.hpp
	Probability
		- See PSI_ProbabilityModule.hpp
//............................................//
Uses:
	PSI_NumericalAnalysis/PSI_MathModule.hpp

Machine Learning Algorithms List:
	Association Rules
		Map Association
		Multimap Association
		Weight Association
	Decision Trees
		Deterministic
		Probabilistic
		Regresive
		Progresive
	Regression
		Linear
		Polynomial
		Expologarithmic
		Trigonometric
		Hiperbolic
		Elliptic
	Support Vector Machines
		Linear
		Nonlinear
	Clustering
		K-Density
	Neural Networks
		Linear NN
		Multilinear NN
		Nonlinear NN
		Non Multilinear NN
		Recursive NN
		Bigraph NN
		Pantograph NN
	Genetic Algorithms
		Swarm G
		Hive G
		Nest G
		Anthill G
	Evolutionary Algorithms
		Search & Destroy
		Optimal Survival
		Cooperative Colony
********************************************* */ 

/* *********************************************
DEFINES:
	PSI_MathModule.hpp
	PSI_StatisticsToolbox.hpp
MACROS:
	CSTD
	PSI_MACHLEARNING
STRUCTS:

ENUMS:

TYPES:

CLASSES:
	PSI_MLDataSeries
	PSI_MLSet
	PSI_MLDevice
********************************************* */ 

#include "../PSI_NumericalAnalysis/PSI_MathModule.hpp"
#include "../PSI_Statistics/PSI_StatisticsToolbox.hpp"

#ifndef CSTD
#define CSTD
	#include <iostream>
	#include <string>
	#include <fstream>
	#include <chrono>
	using namespace std;
	using namespace chrono;
#endif

#ifndef PSI_MACHLEARNING
#define PSI_MACHLEARNING

namespace PSI_MathUtility{
    namespace PSI_MachLearningToolbox{
		static class PSI_MachLearning: public virtual PSI_MathUtilityToolbox{

        }; 
/* ********************************************* 
CLASS:
	PSI_MachLearningDataSeries
PROPERTIES:
	machlearningDataSeries
	machlearningDataSeries_Index
	machlearningDataSeries_Count
METHODS:

OPERATORS:
********************************************* */
template <typename T>
class PSI_MachLearningDataSeries{
	public:
	/* CONSTRUCTOR */
		PSI_MachLearningDataSeries();
		~PSI_MachLearningDataSeries();
	/* ATTRIBUTES */
		void* machlearningDataSeries;
		uint64_t machlearningDataSeries_Index;
		uint64_t machlearningDataSeries_Count;
	/* METHODS */

	/* OPERATORS */

	/* PRIVATE CONTAINER */
	private:
	/* PROTECTED CONTAINER */
	protected:
};

/* ********************************************* 
CLASS:
	PSI_MachLearningSet
PROPERTIES:
	DataSeries
	MachLearningDevice
METHODS:

OPERATORS:
********************************************* */
template <typename T>
class PSI_MachLearningSet{
	public:
	/* CONSTRUCTOR */
		PSI_MachLearningSet();
		~PSI_MachLearningSet();
	/* ATTRIBUTES */
		PSI_MachLearningDataSeries DataSeries;
		PSI_MachLeargningDevice MachLearningDevice;
	/* METHODS */

	/* OPERATORS */

	/* PRIVATE CONTAINER */
	private:
	/* PROTECTED CONTAINER */
	protected:
};


/* ********************************************* 
CLASS:
	PSI_MachLearningDevice
PROPERTIES:

METHODS:
	Association Rules
	Decision Trees
	Regression
	Support Vector Machines
	Clustering
	Neural Networks
	Genetic Algorithms
	Evolutionary Algorithms
OPERATORS:
********************************************* */
class PSI_MachLearningDevice{
	public:
	/* CONSTRUCTOR */
		PSI_MachLearningDevice();
		~PSI_MachLearningDevice();
	/* ATTRIBUTES */

	/* METHODS */

	/* OPERATORS */

	/* PRIVATE CONTAINER */
	private:
	/* PROTECTED CONTAINER */
	protected:
};

	}
}
#endif