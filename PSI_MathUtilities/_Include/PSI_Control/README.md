# Psideralis Math Utilities:  Topology
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public  Progress 0%
## Version
00.00.000.001
## Description
Control toolbox library includes:
    
    Signals
        Impulse
        Step
        Ramp
        Saw
        Triangular
        Cuadratic
        Trigonometric
        Trigonometric Sweep
        Trigonometric Armonic
        Trigonometric Armonic Sweep
        Expologarithmic
        Expologarithmic Sweep
        Expologarithmic Armonic
        Expologarithmic Armonic Sweep
        Stochastic signals
        Probabilistic signals
        Noise
            Gaussian noise
    Frequency Domain
        LaPlace Transform
        Fourier Transform
        Bilineal Transform
    Frecuency,Time Domain
        Wavelet Transform
        Defonic Transform
        Fourier Series
        Defonic Series
    Circuit System Methods (PDE Modelling)
        Circuit Representation
        Discrete Systems & Digital signals
        Continous Systems & Analog signals
        Mixed signal Systems
        Controller Desing
            Mechanical
            Pneumatic
            Hydraulic
            Thermic
            Electric
    State Space Methods (Matrix Modelling)
        Matrix Representation
        Discrete Systems & Digital signals
        Continous Systems & Analog signals
        Mixed signal Systems
        Controller Desing
            Robust
            Optimal
            Adaptative
            Intelligent
            Evolutive
            Reconfigurable (FPGA,FRAG Systems)
    State Machine Methods (Graph Modelling)
        Table, Graph Representation
        Discrete Systems & Digital signals
        Continous Systems & Analog signals
        Mixed signal Systems
        Controller Desing
            Deciditory
            Logistic