/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_ControlToolbox.hpp
Description: Psideralis Control toolbox

Control toolbox library includes:
    -Signals
        -Impulse,Step,Ramp,Square
        -Cuadratic
        -Sinosoidal,Sweep,Armonic
        -Exponential,Sweep,Armonic
        -Logaritmic,Sweep, Armonic
        -Stochastic Signals
        -Noise
    -Frequency Domain
        -LaPlace Transform
        -FFT
        -Bilineal Transform
    -Frecuency,Time Domain
        -Wavelet Transform
        -Fourier Series
        -Expologarithmic Series
    -State Space Methods
        -Matrix Representation
        -Discrete Systems & Digital signals
        -Continous Systems & Analog signals
        -Hybrid Systems
        -Controller Desing
            -Robust
            -Optimal
            -Adaptative
            -Intelligent
            -Evolutive
********************************************* */ 

#ifndef CSTD
#define CSTD
    #include <iostream>
    #include <string>
    #include <fstream>
    #include <chrono>
    using namespace std;
    using namespace chrono;
#endif

#ifndef PSI_CONTROL
#define PSI_CONTROL

namespace PSI_MathUtility
{
    namespace PSI_ControlToolbox
    {
        static class PSI_Control: public virtual PSI_MathUtilityToolbox{

        }; 
    }
}

#endif