/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_UniversalAlgebraModule.hpp
Description: Universal algebra toolkit.

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_UNIVERSAL
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_UNIVERSAL
#define PSI_UNIVERSAL

namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
        namespace PSI_UniversalAlgebraModule{
            static class PSI_UniversalAlgebra: public virtual PSI_MathUtilityModule{
                public:
                PSI_UniversalAlgebra();
                ~PSI_UniversalAlgebra();
                private:
                protected:
            };
        }
    }
}

#endif