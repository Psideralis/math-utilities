/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_TensorModule.hpp
Description: Tensor until order 5 with
basic operators and transformations.

********************************************* */ 

/* *********************************************
DEFINES:
	PSI_TENSOR
MACROS:

STRUCTS:
	TensorArrayO3_s
	TensorMatrixO3_s
	TensorArrayMatrixO3_s
	TensorArrayMatrixVectorArrayO3_s
	TensorArrayMatrixVectorO3_s
	TensorVectorO3_s
	TensorArrayVectorO3_s
	TensorArrayO4_s
	TensorMatrixO4_s
	TensorArrayMatrixO4_s
	TensorArrayMatrixVectorArrayO4_s
	TensorArrayMatrixVectorO4_s
	TensorVectorO5_s
	TensorArrayVectorO5_s
	TensorArrayO5_s
	TensorMatrixO5_s
	TensorArrayMatrixO5_s
	TensorArrayMatrixVectorArrayO5_s
	TensorArrayMatrixVectorO5_s
	TensorVectorO5_s
	TensorArrayVectorO5_s
ENUMS:

TYPES:
	TensorArrayO3_t
	TensorMatrixO3_t
	TensorArrayMatrixO3_t
	TensorArrayMatrixVectorArrayO3_t
	TensorArrayMatrixVectorO3_t
	TensorVectorO3_t
	TensorArrayVectorO3_t
	TensorArrayO4_t
	TensorMatrixO4_t
	TensorArrayMatrixO4_t
	TensorArrayMatrixVectorArrayO4_t
	TensorArrayMatrixVectorO4_t
	TensorVectorO5_t
	TensorArrayVectorO5_t
	TensorArrayO5_t
	TensorMatrixO5_t
	TensorArrayMatrixO5_t
	TensorArrayMatrixVectorArrayO5_t
	TensorArrayMatrixVectorO5_t
	TensorVectorO5_t
	TensorArrayVectorO5_t
CLASSES:
	TENSOR3O
	TENSOR4O
	TENSOR5O
********************************************* */ 

#ifndef PSI_TENSOR
#define PSI_TENSOR


typedef struct TensorArray3O_s{
	uint64_t sizeO1;
	uint64_t sizeO2;
	uint64_t sizeO3;
	double*** TensorArray3O;
	PSI_RET (*init_TensorArray3O_t_s)(int,int);
	PSI_RET (*dinit_TensorArray3O_t_s)(int,int);
	PSI_RET (*zero_TensorArray3O_t_s)(int,int);
	PSI_RET (*alloc_TensorArray3O_t_s)(int,int);
	PSI_RET (*dealloc_TensorArray3O_t_s)(int,int);
} TensorArray3O_t;

typedef struct TensorArray4O_s{
	uint64_t sizeO1;
	uint64_t sizeO2;
	uint64_t sizeO3;
	uint64_t sizeO4;
	double**** TensorArray4O;
	PSI_RET (*init_TensorArray4O_t_s)(int,int);
	PSI_RET (*dinit_TensorArray4O_t_s)(int,int);
	PSI_RET (*zero_TensorArray4O_t_s)(int,int);
	PSI_RET (*alloc_TensorArray4O_t_s)(int,int);
	PSI_RET (*dealloc_TensorArray4O_t_s)(int,int);
} TensorArray4O_t;

typedef struct TensorArray5O_s{
	uint64_t sizeO1;
	uint64_t sizeO2;
	uint64_t sizeO3;
	uint64_t sizeO4;
	uint64_t sizeO5;
	double***** TensorArray5O;
	PSI_RET (*init_TensorArray5O_t_s)(int,int);
	PSI_RET (*dinit_TensorArray5O_t_s)(int,int);
	PSI_RET (*zero_TensorArray5O_t_s)(int,int);
	PSI_RET (*alloc_TensorArray5O_t_s)(int,int);
	PSI_RET (*dealloc_TensorArray5O_t_s)(int,int);
} TensorArray5O_t;

namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
		namespace PSI_TensorAlgebraModule{

static class PSI_TensorAlgebra: public virtual PSI_MathUtilityModule{
    public:
    PSI_TensorAlgebra();
    ~PSI_TensorAlgebra();
    private:
    protected:
};

/* *********************************************
CLASS:
	NAME:

	DESCRIPTION:

	ATRIBUTES:

	METHODS:

	OPERATORS:

********************************************* */ 
template <typename T>
class Tensor3 {
	public:
	/* CONSTRUCTORS */
	Tensor3(){};
	~Tensor3(){};
	Tensor3(const Tensor3 &cpy){};
	/* ATTRIBUTES */
	int order;
	T*** entry;
	/* METHODS */	
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	void empty(){};
	/* OPERATORS */
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator==(const Tensor3 &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator=(const Tensor3 &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator=(const Tensor3* &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator+(const Tensor3 &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator-(const Tensor3 &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator*(const T &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator*(const Tensor3 &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator/(const T &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Tensor3& operator/(const Tensor3 &rhs){};
	private:
	protected:
};

		}
    }
}


#endif