/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_SymbolicAlgebraModule.hpp
Description: A symbolic processor for all the
Psideralis Math-Utilities framework.

Provides results in format for uplot and
ulogik.
********************************************* */ 

/* *********************************************
DEFINES:
    PSI_SYMBOLIC
MACROS:

STRUCTS:
    PSI_symbols_s
ENUMS:
    PSI_algebra_e
    PSI_arithmetics_e
    PSI_control_e
    PSI_geometry_e
    PSI_logic_e
    PSI_machlearning_e
    PSI_numanalysis_e
    PSI_optimization_e
    PSI_statistics_e
    PSI_topology_e
    PSI_categories_e
TYPES:
    PSI_algebra_t
    PSI_arithmetics_t
    PSI_control_t
    PSI_geometry_t
    PSI_logic_t
    PSI_machlearning_t
    PSI_numanalysis_t
    PSI_optimization_t
    PSI_statistics_t
    PSI_topology_t
    PSI_categories_t
    PSI_symbols_t
CLASSES:
    uLTRAShell
    SymbolicProcessor
********************************************* */ 

#ifndef PSI_SYMBOLIC
#define PSI_SYMBOLIC

namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
		namespace PSI_SymbolicAlgebraModule{
static class PSI_SymbolicAlgebra: public virtual PSI_MathUtilityModule{
    public:
    PSI_SymbolicAlgebra();
    ~PSI_SymbolicAlgebra();
    private:
    protected:
};
typedef enum PSI_algebra_e{
    PSI_ABSTRACT,
    PSI_MATRIX,
    PSI_SYMBOLIC,
    PSI_TENSOR,
    PSI_UNIVERSAL,
    PSI_VECTOR,
    PSI_ALGEBRA
} PSI_algebra_t;

typedef enum PSI_arithmetics_e{
    PSI_BINARY,
    PSI_COMPLEX,
    PSI_MODULE,
    PSI_REAL,
    PSI_ARITHMETICS
} PSI_arithmetics_t;

typedef enum PSI_control_e{
    PSI_CONTROL
} PSI_control_t;

typedef enum PSI_geometry_e{
    PSI_GEOMETRY
} PSI_geometry_t;

typedef enum PSI_logic_e{
    PSI_CATEGORIES,
    PSI_COMBINATORICS,
    PSI_Equation,
    PSI_GRAPHS,
    PSI_MODELS,
    PSI_SET,
    PSI_SYSTEMS,
    PSI_LOGIC
} PSI_logic_t;

typedef enum PSI_machlearning_e{
    PSI_MACHINELEARNING
} PSI_machlearning_t;

typedef enum PSI_numanalysis_e{
    PSI_MATH,
    PSI_NUMERICAL,
    PSI_VARIATION,
    PSI_NUMERICALANALYSIS
} PSI_numanalysis_t;

typedef enum PSI_optimization_e{
    PSI_OPTIMIZATION
} PSI_optimization_t;

typedef enum PSI_statistics_e{
    PSI_PROBABILITY,
    PSI_STATISTICS
} PSI_statistics_t;

typedef enum PSI_topology_e{
    PSI_ALGEBRAIC,
    PSI_DIFFERENTIAL,
    PSI_GENERAL,
    PSI_TOPOLOGY
} PSI_topology_t;

typedef enum PSI_categories_e{
    PSI_ALGEBRA = 0,
    PSI_ARITHMETICS = 1,
    PSI_CONTROL = 2,
    PSI_GEOMETRY = 3, 
    PSI_LOGIC = 4,
    PSI_MACHLEARNING = 5,
    PSI_NUMANALYSIS = 6,
    PSI_OPTIMIZATION = 7,
    PSI_STATISTICS = 8,
    PSI_TOPOLOGY = 9
} PSI_categories_t;

typedef struct PSI_symbols_s{
    // LOGICS
    char unq = '!';
    char ext = '∃';
    char unv = '∀';
    char not = '~';
    char and = '&';
    char or  = '|';
    char csq = '⇒';
    char imp = '↔';
    // ARITHMETICS
    char sum = '+';
    char smm = '∑';
    char res = '-';
    char div = '/';
    char mul = '*';
    char pdr = '∏';
    char pow = '^';
    char root = '√';
    char mod = 'mod';
    char prc = '%';
    char rsm = '∓';
    char smr = '±';
    // GEOMETRICS
    char ang = '∠';
    // ALGEBRAICS
    char eq  = '=';
    char neq = '≠';
    char eql = '⇔';
    char cls = '∵';
    char lss = '<';
    char lse = '<=';
    char gtr = '>';
    char gte = '>=';
    char pnt = '⋅';
    char crs = 'x';
    char knc = '⊗';
    // TOPOLOGICS

    // ANALYTICS
    char dlt = '∆';
    char der = 'd';
    char prm = '′';
    char dpm = '″';
    char drv = 'δ';
    char nbl = '∇';
    char lim = 'lim';
    char tnd = '→';
    char intg = '∫';
    char cint = '∮';
    char int2 = '∬';
    char cint2 ='∯';
    char int3 = '∭';
    char cint3 = '∰';
    // MISCELANEOUS
    char inf = '∞';
    char ift = 'ε';
    // STATISTICS

    // OPTIMIZATION

    // MACHINE LEARNING

    // CONTROL
} PSI_symbols_t;

/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Equation{
    public:
    /* CONSTRUCTOR */
    PSI_Equation(){}
    PSI_Equation(string funsymbol){}
    ~PSI_Equation(){}
    ~PSI_Equation(const PSI_Equation &cpy){}
    /* ATRIBUTOS */
    T* member;
    /* MÉTODOS */
    /* OPERADORES */
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_EquationSystem{
    public:
    /* CONSTRUCTOR */
    PSI_EquationSystem(){}
    PSI_EquationSystem(string funsymbol){}
    ~PSI_EquationSystem(){}
    ~PSI_EquationSystem(const PSI_Equation &cpy){}
    /* ATRIBUTOS */
    T* member;
    /* MÉTODOS */
    /* OPERADORES */
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_DiffEquation{
    public:
    /* CONSTRUCTOR */
    PSI_DiffEquation(){}
    PSI_DiffEquation(string funsymbol){}
    ~PSI_DiffEquation(){}
    ~PSI_DiffEquation(const PSI_DiffEquation &cpy){}
    /* ATRIBUTOS */
    T* member;
    /* MÉTODOS */
    /* OPERADORES */
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_DiffEquationSystem{
    public:
    /* CONSTRUCTOR */
    PSI_DiffEquationSystem(){}
    PSI_DiffEquationSystem(string funsymbol){}
    ~PSI_DiffEquationSystem(){}
    ~PSI_DiffEquationSystem(const PSI_Equation &cpy){}
    /* ATRIBUTOS */
    T* member;
    /* MÉTODOS */
    /* OPERADORES */
    private:
    protected:
};
static class uLTRAShell{
    public:
    // Constructor
        uLTRAShell(){};
        ~uLTRAShell(){};
    // Properties
        SymbolicProcessor uLTRAProcessor;
    // Methods
        void start(){
            bool exit = false;
            uLTRAProccesor = new SymbolicProcessor();
            while(!exit){
                exit = uLTRAProccesor.preproc_in();
                uLTRAProccesor.postproc_in();
                exit =  uLTRAProccesor.proc_in();
                switch(exit){
                    case 1:{

                    },
                    default:{
                        break;
                    }                   
                }
                uLTRAProccesor.preproc_out();
                exit =  uLTRAProccesor.postproc_out();
            }
            return;
        };
        void pause(){

        };
        void stop(){

        };
        void exit(){

        };
    // Operators
    private:
    protected:
}

static class SymbolicProcessor{
    public:
    // Constructor
        SymbolicProcessor(){};
        ~SymbolicProcessor(){};
    // Properties
        PSI_symbols_t keywords;
        PSI_categories_t modules;
    // Methods
        void preproc_in(){

        };
        void postproc_in(){

        };
        void proc_in(){
            switch(){
                case modules.PSI_ALGEBRA:{

                },
                case modules.PSI_ARITHMETICS:{

                },
                case modules.PSI_CONTROL:{

                },
                case modules.PSI_GEOMETRY:{

                },
                case modules.PSI_LOGIC:{

                },
                case modules.PSI_MACHLEARNING:{

                },
                case modules.PSI_NUMANALYSIS:{

                },
                case modules.PSI_OPTIMIZATION:{

                },
                case modules.PSI_STATISTICS:{

                },
                case modules.PSI_TOPOLOGY:{

                }
                default: {

                }
            }

        };
        void preproc_out(){

        };
        void postproc_out(){

        };
    // Operators
    private:
    protected:
};   
        }       
    }
}

#endif