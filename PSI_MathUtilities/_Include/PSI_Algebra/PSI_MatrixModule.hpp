/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_MatrixModule.hpp
Description: Matrix in nxn dimensions with
basic operators and transformations.

********************************************* */ 

/* *********************************************
DEFINES:
	PSI_MATRIX
MACROS:

STRUCTS:
	MatrixArray2D_s
	MatrixVectorArray2D_s
	MatrixVector2D_s
	MatrixArray3D_s	
	Matrix3D_s
	MatrixVectorArray3D_s
	MatrixVector3D_s
	MatrixArray4D_s
	MatrixVectorArray4D_s
	MatrixVector4D_s
	Matrix4D_s
	MatrixnD_s
ENUMS:

TYPES:
	MatrixArray2D_t
	MatrixVectorArray2D_t
	MatrixVector2D_t
	MatrixArray3D_t	
	Matrix3D_t
	MatrixVectorArray3D_t
	MatrixVector3D_t
	MatrixArray4D_t
	MatrixVectorArray4D_t
	MatrixVector4D_t
	Matrix4D_t
	MatrixnD_t
CLASSES:
	MATRIX
	MATRIX2D
	MATRIX3D
	MATRIX4D
********************************************* */ 

#include "PSI_VectorModule.hpp"

#ifndef CSTD
#define CSTD
	#include <iostream>
	#include <string>
	#include <fstream>
	#include <chrono>
	#include <typeinfo> 
	using namespace std;
	using namespace chrono;
#endif

#ifndef PSI_MATRIX
#define PSI_MATRIX

typedef struct MatrixArray2D_s{
	uint64_t size_x;
	uint64_t size_y;
	double** MatrixArray2D;
	PSI_RET (*init_MatrixArray2D_s)(int,int);
	PSI_RET (*dinit_MatrixArray2D_s)(int,int);
	PSI_RET (*zero_MatrixArray2D_s)(int,int);
	PSI_RET (*alloc_MatrixArray2D_s)(int,int);
	PSI_RET (*dealloc_MatrixArray2D_s)(int,int);
} MatrixArray2D_t;

typedef struct MatrixVector2D_s{

} MatrixVector2D_t;

typedef struct Matrix2D_s{
	uint64_t size_x;
	uint64_t size_y;
	double* x;
	double* y;
	PSI_RET (*init_Matrix2D_s)(int,int);
	PSI_RET (*dinit_Matrix2D_s)(int,int);
	PSI_RET (*zero_Matrix2D_s)(int,int);
	PSI_RET (*alloc_Matrix2D_s)(int,int);
	PSI_RET (*dealloc_Matrix2D_s)(int,int);
} Matrix2D_t;

typedef struct MatrixArray3D_s{
	uint64_t size_x;
	uint64_t size_y;
	uint64_t size_z;
	double** MatrxiArray3D;
	PSI_RET (*init_MatrixArray3D_s)(int,int);
	PSI_RET (*dinit_MatrixArray3D_s)(int,int);
	PSI_RET (*zero_MatrixArray3D_s)(int,int);
	PSI_RET (*alloc_MatrixArray3D_s)(int,int);
	PSI_RET (*dealloc_MatrixArray3D_s)(int,int);
} MatrixArray3D_t;

typedef struct Matrix3D_s{
	uint64_t size_x;
	uint64_t size_y;
	uint64_t size_z;
	double* x;
	double* y;
	double* z;
	PSI_RET (*init_Matrix3D_s)(int,int);
	PSI_RET (*dinit_Matrix3D_s)(int,int);
	PSI_RET (*zero_Matrix3D_s)(int,int);
	PSI_RET (*alloc_Matrix3D_s)(int,int);
	PSI_RET (*dealloc_Matrix3D_s)(int,int);
} Matrix3D_t;

typedef struct MatrixArray4D_s{
	double x1[4];
	double x2[4]; 
	double x3[4]; 
    double x4[4];
	PSI_RET (*init_MatrixArray4D_s)(int,int);
	PSI_RET (*dinit_MatrixArray4D_s)(int,int);
	PSI_RET (*zero_MatrixArray4D_s)(int,int);
	PSI_RET (*alloc_MatrixArray4D_s)(int,int);
	PSI_RET (*dealloc_MatrixArray4D_s)(int,int);
} MatrixArray4D_t;

typedef struct Matrix4D_s{
	double x1[4];
	double x2[4]; 
	double x3[4]; 
    double x4[4];
	PSI_RET (*init_Matrix3D_s)(int,int);
	PSI_RET (*dinit_Matrix3D_s)(int,int);
	PSI_RET (*zero_Matrix3D_s)(int,int);
	PSI_RET (*alloc_Matrix3D_s)(int,int);
	PSI_RET (*dealloc_Matrix3D_s)(int,int);
} Matrix4D_t;

typedef struct MatrixArrayND_s{
    int* size_n;
	double** MatrixArrayND;
	PSI_RET (*init_MatrixArrayND_s)(int,int);
	PSI_RET (*dinit_MatrixArrayND_s)(int,int);
	PSI_RET (*zero_MatrixArrayND_s)(int,int);
	PSI_RET (*alloc_MatrixArrayND_s)(int,int);
	PSI_RET (*dealloc_MatrixArrayND_s)(int,int);
} MatrixArrayND_t;


namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
		namespace PSI_MatrixAlgebraModule{
static class PSI_MatrixAlgebra: public virtual PSI_MathUtilityModule{
    public:
    PSI_MatrixAlgebra();
    ~PSI_MatrixAlgebra();
    private:
    protected:
};

/* *********************************************
CLASS:

NAME:

DESCRIPTION:

ATRIBUTES:

METHODS:

OPERATORS:

********************************************* */ 
template <typename T>
class Matrix {
	public:
	/* CONSTRUCTORS */
	Matrix(){}
	Matrix(int n, T i){
		this->rows = n;
		this->entry = new Vector<T>[this->n];
		for (int i = 0 ; i < this->rows; i++){	
			this->entry[i] = new Vector<T>(n,0);
		}
		for (int i = 0 ; i < this->rows; i++){
			for (int j = 0 ; j < this->rows; j++){
				this->entry[i]->entry[j] = i;
			}
		}
	}
	Matrix(int n, int m, T i){
		this->rows = n;
		this->columns = m;
		this->entry = new Vector<T>[this->n];
		for (int i = 0 ; i < this->rows; i++){	
			this->entry[i] = new Vector<T>(this->columns,0);
		}
		for (int i = 0 ; i < this->rows; i++){
			for (int j = 0 ; j < this->columns; j++){
				this->entry[i]->vector[j] = i;
			}
		}
	}
	~Matrix(){
		this->empty();
	}
	Matrix(const Matrix &cpy){

	}
	/* ATRIBUTES */
	int rows;
	int columns;
	Vector<T>* entry;
	/* METHODS */
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	void empty(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	void max(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	void min(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	T det(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix cof(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix inv(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	T wrk(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix trn(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	void ord(){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	void krn(Matrix matrix_lhs, Matrix matrix_rhs){};
	
	/* OPERATORS */
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator==(const Matrix &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator=(const Matrix &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator=(const Matrix* &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator+(const Matrix &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator-(const Matrix &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator*(const T &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator*(const Matrix &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator/(const T &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix& operator/(const Matrix &rhs){};
	
	private:
	protected:
};

/* *********************************************
CLASS:

NAME:

DESCRIPTION:

ATRIBUTES:

METHODS:

OPERATORS:

********************************************* */ 
template <typename T>
class Matrix2D {
	public:
	/* CONSTRUCTORS */
	Matrix2D(){};
	~Matrix2D(){};

	/* ATRIBUTES */
	T x[2];
	T y[3];
	/* METHODS */

	/* OPERATORS */
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator==(const Matrix2D &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator=(const Matrix2D &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator=(const Matrix2D* &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator+(const Matrix2D &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator-(const Matrix2D &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator*(const T &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator*(const Matrix2D &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator/(const T &rhs){};
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix2D& operator/(const Matrix2D &rhs){};

	private:
	protected:
};

/* *********************************************
CLASS:

NAME:

DESCRIPTION:

ATRIBUTES:

METHODS:

OPERATORS:

********************************************* */ 
template <typename T>
class Matrix3D {
	public:
	/* CONSTRUCTORS */
	Matrix3D(){};
	~Matrix3D(){};

	/* ATRIBUTES */
	T x[3];
	T y[3];
	T z[3];
	
	/* METHODS */

	/* OPERATORS */
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator==(const Matrix3D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator=(const Matrix3D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/	
	Matrix3D& operator=(const Matrix3D* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator+(const Matrix3D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator-(const Matrix3D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator*(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator*(const Matrix3D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator/(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix3D& operator/(const Matrix3D &rhs){};
	private:
	protected:
};

/* *********************************************
CLASS:

NAME:

DESCRIPTION:

ATRIBUTES:

METHODS:

OPERATORS:

********************************************* */ 
template <typename T>
class Matrix4D {
	public:
	/* CONSTRUCTORS */
	Matrix4D(){};
	~Matrix4D(){};
	
	/* ATTRIBUTES */
	T x1[4];
	T x2[4];
	T x3[4];
	T x4[4];
	/* METHODS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void empty(){

	};
	/* OPERATORS */
	/*
	Name:
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator==(const Matrix4D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator=(const Matrix4D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/	
	Matrix4D& operator=(const Matrix4D* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator+(const Matrix4D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator-(const Matrix4D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator*(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator*(const Matrix4D &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator/(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Matrix4D& operator/(const Matrix4D &rhs){};
	private:
	protected:
};	
		}
	}
}

#endif