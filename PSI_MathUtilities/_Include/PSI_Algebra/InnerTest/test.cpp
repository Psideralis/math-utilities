#include "algebra.hpp"

int main(){
    /* VECTOR TEST 
    
    
    
    */
    Vector2D_t myVector2D;
    myVector2D.x = 2;
    myVector2D.y = 3;
    Vector3D_t myVector3D;
    myVector3D.x = 1;
    myVector3D.y = 4;
    myVector3D.z = -1000;
    Vector4D_t myVector4D;
    myVector4D.x1 = 1;
    myVector4D.x2 = -1000;
    myVector4D.x3 = 9000;
    myVector4D.x4 = 10;
    VectornD_t myVectornD;
    myVectornD.size = 10;
    myVectornD.x = new double[myVectornD.size];
    for (int i = 0 ; i < myVectornD.size; i++){
        myVectornD.x[i] = 0;
        cout << myVectornD.x[i] << endl;
    }
    Vector2D<double> myVector2DClass;
    Vector3D<double> myVector3DClass;
    Vector4D<double> myVector4DClass;
    Vector<double> myVectorClass;
    /* MATRIX TEST 
    
    
    
    */
    Matrix2D_t myMatrix2D;
    myMatrix2D.x[0] = 1;
    myMatrix2D.x[1] = 1;
    myMatrix2D.y[0] = 1;
    myMatrix2D.y[1] = 1;
    Matrix3D_t myMatrix3D;
    myMatrix3D.x[0] = 0;
    Matrix4D_t myMatrix4D;
    MatrixnD_t myMatrixnD;
    myMatrixnD.size = 3;
    myMatrixnD.x = new double*[myMatrixnD.size];
    for (int i = 0 ; i < myMatrixnD.size; i++){
        myMatrixnD.x[i] = new double[myMatrixnD.size];
        for (int j = 0 ; j < myMatrixnD.size; j++){
            myMatrixnD.x[i][j] = 1;
            cout << myMatrixnD.x[i][j] << endl;
        }
    }
    Matrix2D<double> myMatrix2DClass;
    Matrix3D<double> myMatrix3DClass;
    Matrix4D<double> myMatrix4DClass;
    Matrix<double> myMatrixClass;
    /* TENSOR TEST 
    
    
    
    */
    Tensor3_t myTensor3;
    myTensor3.entry = new double**[myTensor3.order];
    for(int i =0; i< myTensor3.order; i++){
        myTensor3.entry[i] = new double*[myTensor3.order];
        for(int j =0; j< myTensor3.order; j++){
            myTensor3.entry[i][j] = new double[myTensor3.order];
            for(int k =0; k< myTensor3.order; k++){
                myTensor3.entry[i][j][k] = -1;
                cout << myTensor3.entry[i][j][k] << endl;
            }
        }
    }
    Tensor3<double> myTensor3Class;
    return 0;
}