/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_AbstractAlgebraModule.hpp
Description: Abstract algebra
********************************************* */ 

/* *********************************************
DEFINES:
    ABSTRACT

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_ABSTRACT
#define PSI_ABSTRACT

namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
        namespace PSI_AbstractAlgebraModule{

static class PSI_AbstractAlgebra: public virtual PSI_MathUtilityModule{
    public:
    PSI_AbstractAlgebra();
    ~PSI_AbstractAlgebra();
    private:
    protected:
};

        }
    }
}

#endif