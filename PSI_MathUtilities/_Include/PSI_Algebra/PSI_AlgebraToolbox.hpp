/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_AlgebraToolbox.hpp
Description: Psideralis Algebra Toolkit

        - Enumeration
        - Operators
        - Transformations
        - Functions
        - Series
********************************************* */ 

#ifndef CSTD
#define CSTD
    #include <iostream>
    #include <string>
    #include <fstream>
    #include <chrono>
    using namespace std;
    using namespace chrono;
#endif

#ifndef PSI_ALGEBRA
#define PSI_ALGEBRA

#include "PSI_LogicToolbox.hpp"
#include "PSI_AbstractAlgebraModule.hpp"
#include "PSI_MatrixModule.hpp"
#include "PSI_SymbolicAlgebraModule.hpp"
#include "PSI_TensorModule.hpp"
#include "PSI_UniversalAlgebraModule.hpp"
#include "PSI_VectorModule.hpp"

namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
        static class PSI_Algebra: public virtual PSI_MathUtilityToolbox{

        };
    }
}

#endif