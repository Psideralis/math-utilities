# Psideralis Math Utilities:  Algebra
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public - Progress 0%
## Version
00.00.000.001
## Description
Algebra toolbox library includes:

    Symbolic systems
        Linear & Multilinear algebra
            Scalar & Vector & Tensor algebra
            Equations & Inequations
            Constant & Variable Coefficient
            Monovariable & Multivariable
            Constrained & Unconstrained
            Differential & Variational
        NonLinear & NonMultilinear algebra 
        Matrix algebra
            Range
            Inverse
            Traspose
            Conjugate
            Determinant
            Minor
            Autoadjoint
            Eigen values
            Eigen vectors
            Solvers
    Abstract algebra
        Set (Natural, Integer, Rational, Real, Complex)
        Unary structures:
            Magma
            Semigroup  
            Monoid  
            Group   
        Binary structures:
            Conmutative Group
            Ring
            Unitary Ring
            Body
        N-ary structures:
            Module
            Reticule
            Vector Space
            Tensor Space
    Computational Algebra
        Automata
        Finite State Machines
        Lambda Calculus
    Universal algebra
        Arithmetic Algebra
        Geometric Algebra
        Analytic Algebra
        Synthetic Algebra
        Statistical Algebra
    Categories
        Functors
        Lattices
        Isomorphisms