/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_VectorModule.hpp
Description: Vector in n dimensions with
basic operators and transformations.

********************************************* */ 

/* *********************************************
DEFINES:
	PSI_VECTOR
MACROS:

STRUCTS:
	VectorArray2D_s
	Vector2D_s
	VectorArray3D_s
	Vector3D_s
	VectorArray4D_s
	Vector4D_s
	VectorArrayND_s
ENUMS:

TYPES:
	VectorArray2D_t
	Vector2D_t
	VectorArray3D_t
	Vector3D_t
	VectorArray4D_t
	Vector4D_t
	VectorArrayND_t
CLASSES:
	VECTOR
	VECTOR2D
	VECTOR3D
	VECTOR4D
********************************************* */ 

#ifndef PSI_VECTOR
#define PSI_VECTOR

typedef struct VectorArray2D_s{
	double VectorArray2D[2];
	PSI_RET (*init_VectorArray2D_s)(int,int);
	PSI_RET (*dinit_VectorArray2D_s)(int,int);
	PSI_RET (*zero_VectorArray2D_s)(int,int);
	PSI_RET (*alloc_VectorArray2D_s)(int,int);
	PSI_RET (*dealloc_VectorArray2D_s)(int,int);
} VectorArray2D_t;

typedef struct Vector2D_s{
	double x;
	double y; 
	PSI_RET (*init_VectorArray2D_s)(int,int);
	PSI_RET (*dinit_VectorArray2D_s)(int,int);
	PSI_RET (*zero_VectorArray2D_s)(int,int);
	PSI_RET (*alloc_VectorArray2D_s)(int,int);
	PSI_RET (*dealloc_VectorArray2D_s)(int,int);
} Vector2D_t;

typedef struct VectorArray3D_s{
	double VectorArray2D[3];
	PSI_RET (*init_VectorArray2D_s)(int,int);
	PSI_RET (*dinit_VectorArray2D_s)(int,int);
	PSI_RET (*zero_VectorArray2D_s)(int,int);
	PSI_RET (*alloc_VectorArray2D_s)(int,int);
	PSI_RET (*dealloc_VectorArray2D_s)(int,int);
} VectorArray3D_t;

typedef struct Vector3D_s{
	double x;
	double y; 
	double z; 
	PSI_RET (*init_Vector3D_s)(int,int);
	PSI_RET (*dinit_Vector3D_s)(int,int);
	PSI_RET (*zero_Vector3D_s)(int,int);
	PSI_RET (*alloc_Vector3D_s)(int,int);
	PSI_RET (*dealloc_Vector3D_s)(int,int);
} Vector3D_t;

typedef struct VectorArray4D_s{
	double VectorArray2D[4];
	PSI_RET (*init_VectorArray4D_s)(int,int);
	PSI_RET (*dinit_VectorArray4D_s)(int,int);
	PSI_RET (*zero_VectorArray4D_s)(int,int);
	PSI_RET (*alloc_VectorArray4D_s)(int,int);
	PSI_RET (*dealloc_VectorArray4D_s)(int,int);
} VectorArray4D_t;

typedef struct Vector4D_s{
	double x;
	double y; 
	double z; 
    double w;
	PSI_RET (*init_Vector4D_s)(int,int);
	PSI_RET (*dinit_Vector4D_s)(int,int);
	PSI_RET (*zero_Vector4D_s)(int,int);
	PSI_RET (*alloc_Vector4D_s)(int,int);
} Vector4D_t;

typedef struct VectorArrayND_s{
    int size;
	double* VectorArrayND;
	PSI_RET (*init_VectorArrayND_s)(int,int);
	PSI_RET (*dinitVectorArrayND_s)(int,int);
	PSI_RET (*zero_VectorArrayND_s)(int,int);
	PSI_RET (*alloc_VVectorArrayND_s)(int,int);
	PSI_RET (*dealloc_VectorArrayND_s)(int,int);
} VectorArrayND_t;

namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
		namespace PSI_VectorAlgebra{
static class PSI_VectorAlgebra: public virtual PSI_MathUtilityModule{
    public:
    PSI_VectorAlgebra();
    ~PSI_VectorAlgebra();
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
		VECTOR
	DESCRIPTION:
		N-dimensional vector.
	ATRIBUTES:
		size: size of the vector.
		entry: each one of the entries of the
			vector.
	METHODS:
		empty: empty the vector.
		min: minimun value.
		max: maximum value.
		traslation: traslate the vector.
		rotation: rotate the vector.
		project: project the vector to
			respective dimension.
		magnitude: return the magnitude of
			the vector.
		phase: return the phase of the vector.
	OPERATORS:
		==: comparation
		=: asignation
		+: addition
		-: substraction
		*: multiplication
		/: division
********************************************* */ 

template <typename T>
class Vector {
	public:
	/* CONSTRUCTORS */
	Vector(){};
	Vector(char mode){
		if (mode == 'c'){
			T entry;
			uint size;
			std::cout << "Size of the vector:";
			std::cin >> size;
			this->size = size;
			this->vector = new T[this->size];
			for (int i = 0; i < this->size; i++){
				std::cout << "Entry " << i << ": "<< std::endl;
				std::cin >> entry;
				this->vector[i] = entry;
			}
		}else{
			std::cout << "Non valid mode.";
		}
	};
	Vector(int size){
		this->size = size;
		this->entry = new T[this->size];
		for (int i = 0; i < this->size; i++){
			std::cout << "Entry " << i << ": "<< std::endl;
			std::cin >> entry;
			this->entry[i] = entry;
		}
	};
	Vector(int n, T i){
		this->size = n;
		this>entry = new T[this->size];
		for (int j = 0; j < this->size; j++){
			this->entry[j] = i;
		}
	};
	Vector(int n, T* i){
		this->size = n;
		this>entry = new T[this->size];
		for (int j = 0; j < this->size; j++){
			this->entry[j] = i[j];
		}
	};
	~Vector(){
		this->empty();
		this->entry = NULL;
		this->size = 0;
	};
	Vector(const Vector &cpy){
		this->empty();
		this->entry = NULL;
		this->size = cpy->size;
		this->entry = new T[this->size];
		for (int i = 0; i < this->size; i++){
			this->entry[i]=cpy->entry[i];
		}
	};
	/* ATTRIBUTES */
	uint size;
	T* entry;
	/* METHODS */	
	void empty(){
		if (entry != NULL){

		}else{
			for (int j = 0; j < this->size; j++){
				this->entry[j] = 0;
			}
		}
	};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void max();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void min();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void traslate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void rotate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void project();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void magnitude();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void phase();
	/* OPERATORS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator==(const Vector<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator=(const Vector<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator=(const Vector<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator+(const Vector<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator+(const Vector<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator-(const Vector<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator-(const Vector<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator*(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator*(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator*(const Vector<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator*(const Vector<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator/(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator/(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator/(const Vector<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector<T>& operator/(const Vector<T>* &rhs){};
	private:
	protected:
};

/* *********************************************
CLASS:
	NAME:
		VECTOR2D
	DESCRIPTION:

	ATRIBUTES:
		x:
		y:
	METHODS:
		empty: empty the vector.
		min: minimun value.
		max: maximum value.
		traslation: traslate the vector.
		rotation: rotate the vector.
		project: project the vector to
			respective dimension.
	OPERATORS:
		==: comparation
		=: asignation
		+: addition
		-: substraction
		*: multiplication
		/: division
********************************************* */ 
template <typename T>
class Vector2D : Vector<T> {
	public:
	/* CONSTRUCTORS */
	Vector2D(){};
	Vector2D(char mode){
		if (mode == 'c'){
			std::cout << "Entry x: "<< std::endl;
			std::cin >> this->x;
			std::cout << "Entry y: "<< std::endl;
			std::cin >> this->y;
		}else{
			std::cout << "Non valid mode.";
		}
	};
	Vector2D(T x, T y){
		this->x = x;
		this->y = y;
	};
	~Vector2D(){
		this->empty();
	};
	Vector2D(const Vector2D &cpy){
		this->empty();
		this->x = cpy->x;
		this->y = cpy->y;
	};
	/* ATTRIBUTES */
	T x;
	T y;
	/* METHODS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void empty(){
		this->x = 0;
		this->y = 0;
	};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void max();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void min();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void traslate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void rotate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void project();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void magnitude();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void phase();
	/* OPERATORS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator==(const Vector2D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator=(const Vector2D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator=(const Vector2D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator+(const Vector2D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator+(const Vector2D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator-(const Vector2D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator-(const Vector2D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator*(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator*(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator*(const Vector2D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator*(const Vector2D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator/(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator/(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator/(const Vector2D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector2D<T>& operator/(const Vector2D<T>* &rhs){};
	private:
	protected:
};

/* *********************************************
CLASS:
	NAME:
		VECTOR3D
	DESCRIPTION:

	ATRIBUTES:
		x:
		y:
		z:
	METHODS:
		empty: empty the vector.
		min: minimun value.
		max: maximum value.
		traslation: traslate the vector.
		rotation: rotate the vector.
		project: project the vector to
			respective dimension.
	OPERATORS:
		==: comparation
		=: asignation
		+: addition
		-: substraction
		*: multiplication
		/: division
********************************************* */ 
template <typename T>
class Vector3D{
	public:
	/* CONSTRUCTORS */
	Vector3D(){};
	Vector3D(char mode){
		if (mode == 'c'){
			std::cout << "Entry x: "<< std::endl;
			std::cin >> this->x;
			std::cout << "Entry y: "<< std::endl;
			std::cin >> this->y;
			std::cout << "Entry z: "<< std::endl;
			std::cin >> this->z;
		}else{
			std::cout << "Non valid mode.";
		}
	};
	Vector3D(T x, T y, T z){
		this->x = x;
		this->y = y;
		this->z = z;
	};
	~Vector3D(){
		this->empty();
	};
	Vector3D(const Vector3D &cpy){
		this->empty();
		this->x = cpy->x;
		this->y = cpy->y;
		this->z = cpy->z;
	};
	/* ATTRIBUTES */
	T x;
	T y;
	T z;
	/* METHODS */
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void empty(){
		this->x = 0;
		this->y = 0;
		this->z = 0;
	};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void max();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void min();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void traslate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void rotate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void project();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void magnitude();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void phase();
	/* OPERATORS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator==(const Vector3D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator=(const Vector3D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator=(const Vector3D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator+(const Vector3D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator+(const Vector3D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator-(const Vector3D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator-(const Vector3D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator*(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator*(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator*(const Vector3D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator*(const Vector3D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator/(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator/(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator/(const Vector3D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector3D<T>& operator/(const Vector3D<T>* &rhs){};
	private:
	protected:
};

/* *********************************************
CLASS:
	NAME:

	DESCRIPTION:

	ATRIBUTES:
		x1:
		x2:
		x3:
		x4:
	METHODS:
		empty: empty the vector.
		min: minimun value.
		max: maximum value.
		traslation: traslate the vector.
		rotation: rotate the vector.
		project: project the vector to
			respective dimension.
	OPERATORS:
		==: comparation
		=: asignation
		+: addition
		-: substraction
		*: multiplication
		/: division
********************************************* */ 
template <typename T>
class Vector4D{
	public:
	/* CONSTRUCTORS */
	Vector4D(){};
	Vector4D(char mode){
		if (mode == 'c'){
			std::cout << "Entry x1: "<< std::endl;
			std::cin >> this->x1;
			std::cout << "Entry x2: "<< std::endl;
			std::cin >> this->x2;
			std::cout << "Entry x3: "<< std::endl;
			std::cin >> this->x3;
			std::cout << "Entry x4: "<< std::endl;
			std::cin >> this->x4;
		}else{
			std::cout << "Non valid mode.";
		}
	};
	Vector4D(T x1, T x2,T x3, T x4){
		this->x1 = x1;
		this->x2 = x2;
		this->x3 = x3;
		this->x4 = x4;
	};
	~Vector4D(){
		this->empty();
	};
	Vector4D(const Vector4D &cpy){
		this->empty();
		this->x1 = cpy->x1;
		this->x2 = cpy->x2;
		this->x3 = cpy->x3;
		this->x4 = cpy->x4;
	};
	/* ATTRIBUTES */
	T x1;
	T x2;
	T x3;
	T x4;
	/* METHODS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void empty(){
		this->x1 = 0;
		this->x2 = 0;
		this->x3 = 0;
		this->x4 = 0;
	};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void max();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void min();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void traslate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void rotate();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void project();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void magnitude();
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	void phase();
	/* OPERATORS */	
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator==(const Vector4D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator=(const Vector4D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator=(const Vector4D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator+(const Vector4D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator+(const Vector4D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator-(const Vector4D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator-(const Vector4D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator*(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator*(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator*(const Vector4D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator*(const Vector4D<T>* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator/(const T &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator/(const T* &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator/(const Vector4D<T> &rhs){};
	/*
	Name:	
	Description:
	Input:
	Output:
	Example:
	*/
	Vector4D<T>& operator/(const Vector4D<T>* &rhs){};
	private:
	protected:
};
		} 		
	}
}

#endif