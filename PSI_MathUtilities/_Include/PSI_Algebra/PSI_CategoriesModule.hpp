/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_CategoriesModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 
#ifndef CATEGORIES
#define CATEGORIES

namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_CategoriesModule
        {
static class PSI_Categories: public virtual PSI_MathUtilityModule{
    public:
    PSI_Categories();
    ~PSI_Categories();
    private:
    protected:
};
        }
    }
} 

#endif