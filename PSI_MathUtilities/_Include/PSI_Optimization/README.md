# Psideralis Math Utilities: Optimizations
## Author
Psideralis
## License
sideralis Public Commune License
## Status
Public - Progress 0%
## Version
00.00.000.001
## Description
Optimization toolbox library includes:

    Algebraic    
        Linear
        Nonlinear
        Multilinear
        Nonmulilinear
    Geometrical
        Topic
        Manifold
    Combinatorial
        Combination
        Order
        Permutation
    Analytical
        Differential
        Variational