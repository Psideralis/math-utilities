/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: optimization.hpp
Description: A basic optimization toolbox 
library.
********************************************* */ 

/* *********************************************
DEFINES:
	PSI_OPTIMIZATION
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

METHODS:

********************************************* */ 


#ifndef PSI_OPTIMIZATION
#define PSI_OPTIMIZATION

#ifndef CSTD
#define CSTD
	#include <iostream>
	#include <string>
	#include <fstream>
	#include <chrono>
	using namespace std;
	using namespace chrono;
#endif

namespace PSI_MathUtility
{
    namespace PSI_OptimizationToolbox
    {
		static class PSI_Optimization: public virtual PSI_MathUtilityToolbox{

        };  
    }
}
 
#endif