# Psideralis Math Utilities: Statistics
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public 	- Progress 20%
## Version
00.00.000.001
## Description
Statistics toolbox library for:
	
	Descriptive Statistics {Position, Dispersion, Form}
		Position measures
			Means
				Weighted
				Partial
				Absolute
				Composite
				Distributed
			Medians
			Modes
			Correlation
		Dispersion measures
			Deviations
				Weighted
				Partial
				Absolute
				Composite
				Distributed
			Variance
		Form measures
			Symmetry
			Grid
			Weigthed
	Inferential Statistics {Differential Statistics (Tim	Space), Variational Statistics (To	Bottom)}
		Normality measures
		Marginality measures
		Exception measures.
		Error measures
		Especulation measures
		Anomaly measures
		Singularity measures
		Metaerror
		Alterance measures
		Paranomy measures
	Hypothesis falsification
		Deductive
		Inductive
		Abductive
		Probabilisitic
	Probability
		Probability Functions
		Conditional probability
		Bayesian networks
			Time series
			Space series
			Timespace series
	Machine Learning:
		Analytic Statistics
			Exactness, Precisedness Measures
		Predictive Statistics
			Correctedness, Faultedness Measures
		Prescriptive Statistics
			Sucessfullness, Failuredness Measures
		Optimization Statistics
			Eficiency (Space), Eficacy (Time) Measures
		Metamodelling Statistics
			Autonomy, Dependance Measures