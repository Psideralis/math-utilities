#include "PSI_StatisticsToolbox.hpp"
using namespace PSI_MathUtility::PSI_StatisticsToolbox;

int main(int argc, char const *argv[])
{
    PSI_StatisticsSet* myWorkbench = new PSI_StatisticsSet();
    myWorkbench->DataSeries<double>.statisticsDataSeries_Data = new double[20];
    myWorkbench->DataSeries<double>.statisticsDataSeries_Dimension = 1;
    myWorkbench->DataSeries<double>.statisticsDataSeries_Size = new uint64_t[myWorkbench->DataSeries<double>.statisticsDataSeries_Dimension];
    myWorkbench->DataSeries<double>.statisticsDataSeries_Size[0] = 20;
    return 0;
}