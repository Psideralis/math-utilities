/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: probability.hpp
Description: A basic probability toolbox library.
//............................................//
For:
	Analytical, Predictive & Prescriptive 
		Statistics or Machine Learning
		- See PSI_MachineLearningToolbox.hpp
	Statistics
		- See PSI_StatisticsToolbox.hpp
//............................................//
Uses:
	PSI_NumericalAnalysis/PSI_MathModule.hpp
		- See Psideralis Math Utilities.
********************************************* */ 

/* *********************************************
INCLUDES:

DEFINES:
	CSTD
	PSI_PROBABILITY
STRUCTS:

ENUMS:

TYPES:

CLASSES:
	PSI_ProbabilitySet
	PSI_ProbabilityEventSeries
	PSI_ProbabilityDevice
METHODS:
	PSI_ProbabilityDevice
		Probability Function Estimator
		Probability Function
		Probability Function Sampler
		Metaprobability Function Sampler
		Probability Function Sampler Estimator
		Metaprobability Function Sampler Estimator
		Probability Function Sampler Metaestimator
		Metaprobability Function Sampler Metaestimator
		Probability Density
		Probability Situ 		(Space depentant probability)
		Probability Tempo 		(Time depentant probability)
		Probability Momenta 	(Velocity dependant probability)
		Probability Incidence 	(Incidence dependant probability)
		Probability Frequency 	(Fequency dependant probability)
		Probability Matter 		(Matter dependant probability)
		Probability Energy 		(Energy dependant probability
		Bayesian Theorem
			Monoconditional Probability
		 	Polyconditional Probability
		Bayesian Networks
			Monoconditional Probability Network
		 	Polyconditional Probability Network
		Probability Error 		(Included variables)
		Probability Anomaly 	(Non-included variables)
		Probability Singularity (Undetermined variables)
********************************************* */ 

#ifndef CSTD
#define CSTD
	#include <iostream>
	#include <string>
	#include <fstream>
	#include <chrono>
	#include <typeinfo> 
	using namespace std;
	using namespace chrono;
#endif

#ifndef PSI_PROBABILITY
#define PSI_PROBABILITY

namespace PSI_MathUtility{
    namespace PSI_StatisticsToolbox{
		namespace PSI_ProbabilityModule{


/* ********************************************* 
CLASS:

PROPERTIES:

METHODS:

OPERATORS:
********************************************* */
class PSI_ProbabilitySet{
	/* PRIVATE CONTAINER */
	private:
		/* ********************************************* 
		CLASS:

		PROPERTIES:

		METHODS:

		OPERATORS:
		********************************************* */
		template <typename T>
		class PSI_ProbabilityEventSeries{
			public:
			/* CONSTRUCTOR */
				PSI_ProbabilityEventSeries();
				~PSI_ProbabilityEventSeries();
			/* ATTRIBUTES */
				void* probabilityEventContainer;
				uint64_t probabilityEvent_Index;
				uint64_t probabilityEvent_Count;
			/* METHODS */

			/* OPERATORS */

			/* PRIVATE CONTAINER */
			private:
			/* PROTECTED CONTAINER */
			protected:
		};
		/* ********************************************* 
		CLASS:

		PROPERTIES:

		METHODS:

		OPERATORS:
		********************************************* */
		template <typename T>
		class PSI_ProbabilityDevice {
			public:
			/* CONSTRUCTOR */
				ProbabilityDevice();
				~ProbabilityDevice();
			/* ATTRIBUTES */
		
			/* METHODS */

			/* OPERATORS */

			/* PRIVATE CONTAINER */
			private:
			/* PROTECTED CONTAINER */
			protected:
		};
	/* PROTECTED CONTAINER */
	protected:
	/* PUBLIC CONTAINER */
	public:
	/* CONSTRUCTOR */
		PSI_ProbabilitySet();
		~PSI_ProbabilitySet();
	/* ATTRIBUTES */
		template <typename T>
		static PSI_ProbabilityEventSeries<T> EventSeries;
		template <typename T>
		static PSI_ProbabilityDevice<T> ProbabilityDevice;
	/* METHODS */

	/* OPERATORS */
};
		}
	}
}
#endif