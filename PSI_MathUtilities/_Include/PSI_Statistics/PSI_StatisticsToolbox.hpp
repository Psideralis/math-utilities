/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_StatisticsToolbox.hpp
Description: A statistics toolbox library.
//............................................//
For:
	Analytical, Predictive, Prescriptive 
		& Optimization Statistics or 
		Machine Learning
		- See PSI_MachineLearningToolbox.hpp
	Probability
		- See PSI_ProbabilityModule.hpp
//............................................//
Uses:
	PSI_NumericalAnalysis/PSI_MathModule.hpp
********************************************* *

/* *********************************************
INCLUDES:
	PSI_MathModule.hpp
	PSI_ProbabilityModule.hpp
DEFINES:
	CSTD
	PSI_STATISTICS
STRUCTS:

ENUMS:

TYPES:

CLASSES:
	PSI_StatisticsSet
	PSI_StatisticsDataSeries
	PSI_StatisticsDevice
METHODS:
********************************************* */ 


//#include "../PSI_NumericalAnalysis/PSI_MathModule.hpp"
#include "PSI_ProbabilityModule.hpp"
#include <cmath>

#ifndef CSTD
#define CSTD
	#include <iostream>
	#include <string>
	#include <fstream>
	#include <chrono>
	#include <typeinfo> 
	using namespace std;
	using namespace chrono;
#endif

#ifndef PSI_STATISTICS
#define PSI_STATISTICS

#define PSI_Static static unsigned long long double

namespace PSI_MathUtility{
    namespace PSI_StatisticsToolbox{

/* ********************************************* 
CLASS:
	PSI_StatisticsSet
PROPERTIES:
	PSI_StatisticsDataSeries
	PSI_StatisticsDevice
METHODS:

OPERATORS:
********************************************* */
class PSI_StatisticsSet{
	/* PRIVATE CONTAINER */
	private:
		/* ********************************************* 
	 	INTERNAL CLASS:
			PSI_StatisticsDataSeries
		PROPERTIES:
			statisticsDataSeries
			statisticsDataSeries_Dimension
			statisticsDataSeries_Size
		METHODS:

		OPERATORS:
		********************************************* */
		template <typename T>
		class PSI_StatisticsDataSeries{
			/* PUBLIC CONTAINER */
			public:
			/* CONSTRUCTOR */
				PSI_StatisticsDataSeries();
				~PSI_StatisticsDataSeries();
			/* ATTRIBUTES */
				T* statisticsDataSeries_Data;
				uint64_t statisticsDataSeries_Dimension;
				uint64_t* statisticsDataSeries_Size;
			/* METHODS */
		
			/* OPERATORS */

			/* PRIVATE CONTAINER */
			private:
			/* PROTECTED CONTAINER */
			protected:
		};
		/* ********************************************* 
		INTERNAL CLASS:
			PSI_StatisticsDevice
		PROPERTIES:

		METHODS:

		OPERATORS:
		********************************************* */
		template <typename T>
		class PSI_StatisticsDevice {
			public:	
				/* CONSTRUCTORS */
				PSI_StatisticsDevice();
				~PSI_StatisticsDevice();
				/* ATTRIBUTES */

			/* METHODS */
			/* ****************************************************
			
				DESCRIPTIVE STATISTICS
					POSITION MEASURES
					DISPERSION MEASURES
					FORM MEASURES
			
			**************************************************** */ 
			/*
				POSITION MEASURES
					MEANS
						PARTIAL MEAN
						ABSOLUTE MEAN
						COMPOSITIVE MEAN
						DISTRIBUTED MEAN
					MEDIANS
					MODES
			*/
			/*
				MEANS
			*/
			PSI_Static arithmetic_mean(int size, int* set){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += set[i];
				}
				return result/size;
			};
			PSI_Static arithmeticPartial_mean(int* size, int** set){
				int result = 0;
				int output = 0;
				for (size_t i = 0; i < size[0]; i++)
				{
					for (size_t j = 0; j < size[1]; j++)
					{
						result += set[i][j];
					}
					output += result/size[1];
				}
				return output/size[0];
			};
			PSI_Static arithmeticAbsolute_mean(int* size, int** set){
				int result = 0;
				for (size_t i = 0; i < size[0]; i++)
				{
					for (size_t j = 0; j < size[1]; j++)
					{
						result += set[i][j];
					}
				}
				return result/size[0]*size[1];
			};
			PSI_Static arithmeticComposite_mean(int* size, int** set){
				int result = 0;
				
				return (arithmetic_mean(size[0],set[0])+arithmetic_mean(size[1],set[1]))/2;
			};
			PSI_Static[2] arithmeticDistributed_mean(int* size, int** set){
				unsigned long long double[2] result;
				result[0] = arithmetic_mean(size[0],set[0]);
				result[1] = arithmetic_mean(size[1],set[1]);
				return result;
			};
			PSI_Static harmonic_mean(int size, int* set){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += set[i];
				}
				return 1/result;
			};
			PSI_Static powerpolynomic_mean(int size, int* set, int p){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(set[i],p);
				}
				return pow(result/size,1/p);
			};
			PSI_Static quadraticpolynomic_mean(int size, int* set){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(set[i],2);
				}
				return pow(result/size,1/2);
			};
			PSI_Static rootpolynomic_mean(int size, int* set){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(set[i],1/2);
				}
				return pow(result/size,2);
			};
			PSI_Static powergeometric_mean(int size, int* set, int p){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result *= pow(set[i],p);
				}
				return pow(result,1/(p*size));
			};
			PSI_Static quadraticgeometric_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result *= pow(set[i],2);
				}
				return pow(result,1/(2*size));
			};
			PSI_Static rootgeometric_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result *= pow(set[i],1/2);
				}
				return pow(result,2/size);
			};
			PSI_Static polynomicpowerpolynomic_mean(int size, int* set, int p){
				int result = 0;
				int output = 0;
				for (size_t i = 0; i < p; i++)
				{
					for (size_t j = 0; j < size; j++)
					{
						result += pow(set[i],i);
					}
					output += result/size;
				}
				return output/p;
			};
			PSI_Static polynomicquadraticpolynomic_mean(){
				
			};
			PSI_Static polynomicrootpolynomic_mean(){
				
			};
			PSI_Static geometricpowerpolynomic_mean();
			PSI_Static geometricquadraticpolynomic_mean();
			PSI_Static geometricrootpolynomic_mean();
			PSI_Static polynomicpowergeometric_mean();
			PSI_Static polynomicquadraticgeometric_mean();
			PSI_Static polynomicrootgeometric_mean();
			PSI_Static geometricpowergeometric_mean();
			PSI_Static geometricquadraticgeometric_mean();
			PSI_Static geometricrootgeometric_mean();
			PSI_Static nexponential_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += exp(set[i],10);
				}
				return log(result,10);
			};
			PSI_Static nlogarithmic_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += log(set[i],10);
				}
				return exp(result,10);
			};
			PSI_Static bexponential_mean();
			PSI_Static blogarithmic_mean();
			PSI_Static exponential_mean();
			PSI_Static logarithmic_mean();
			PSI_Static strigonometric_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(sen(set[i]),2);
				}
				return pow(result,1/2);
			};
			PSI_Static ctrigonometri_mean();
			PSI_Static rtrigonometric_mean();
			PSI_Static shiperbolic_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(senh(set[i]),2);
				}
				return pow(result,1/2);
			};
			PSI_Static chiperbolic_mean();
			PSI_Static rhiperbolic_mean();
			PSI_Static selliptic_mean();
			PSI_Static celliptic_mean();
			PSI_Static relliptic_mean();
			PSI_Static sparabollic_mean(){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(PSI_senp(set[i]),2);
				}
				return pow(result,1/2);
			};
			PSI_Static cparabollic_mean();
			PSI_Static rparabollic_mean();
			/*
				MEDIANS
			*/
			PSI_Static median();
			/*
				MODES
			*/
			PSI_Static mode();
			/*
				DISPERSION MEASURES
					VARIANCE'
						PARTIAL
						ABSOLUTE
						COMPOSITE
					DISPERSION
			*/
			/*
				VARIANCE
			*/
			PSI_Static standard_variance(int size, int* set){
				int result = 0;
				for (size_t i = 0; i < size; i++)
				{
					result += pow(arithmetic_mean(size, set) - set[i],2);
				}
				return result;
			};	
			PSI_Static arithmetic_variance();
			PSI_Static harmonic_variance();
			PSI_Static powerpolynomic_variance();
			PSI_Static quadraticpolynomic_variance();
			PSI_Static rootpolynomic_variance();
			PSI_Static powergeometric_variance();
			PSI_Static quadraticgeometric_variance();
			PSI_Static rootgeometric_variance();
			PSI_Static polynomicpowerpolynomic_variance();
			PSI_Static polynomicrootpolynomic_variance();
			PSI_Static geometricpowerpolynomic_variance();
			PSI_Static geometricrootpolynomic_variance();
			PSI_Static polynomicpowergeometric_variance();
			PSI_Static polynomicrootgeometric_variance();
			PSI_Static geometricpowergeometric_variance();
			PSI_Static geometricrootgeometric_variance();
			PSI_Static quadraticpowerpolynomic_variance();
			PSI_Static rootpowerpolynomic_variance();
			PSI_Static quadraticpowergeometric_variance();
			PSI_Static rootpowergeometric_variance();
			PSI_Static nexponential_variance();
			PSI_Static nlogarithmic_variance();
			PSI_Static bexponential_variance();
			PSI_Static blogarithmic_variance();
			PSI_Static exponential_variance();
			PSI_Static logarithmic_variance();
			PSI_Static strigonometric_variance();
			PSI_Static ctrigonometri_variance();
			PSI_Static rtrigonometric_variance();
			PSI_Static shiperbolic_variance();
			PSI_Static chiperbolic_variance();
			PSI_Static rhiperbolic_variance();
			PSI_Static selliptic_variance();
			PSI_Static celliptic_variance();
			PSI_Static relliptic_variance();
			PSI_Static sparabollic_variance();
			PSI_Static cparabollic_variance();
			PSI_Static rparabollic_variance();
			/*
				DISPERSION
			*/
			PSI_Static standard_dispersion(int size, int* set){
				return pow(standard_variance(size, set),1/2);
			};
			PSI_Static arithmetic_dispersion();
			PSI_Static harmonic_dispersion();
			PSI_Static powerpolynomic_dispersion();
			PSI_Static quadraticpolynomic_dispersion();
			PSI_Static rootpolynomic_dispersion();
			PSI_Static powergeometric_dispersion();
			PSI_Static quadraticgeometric_dispersion();
			PSI_Static rootgeometric_dispersion();
			PSI_Static polynomicpowerpolynomic_dispersion();
			PSI_Static polynomicrootpolynomic_dispersion();
			PSI_Static geometricpowerpolynomic_dispersion();
			PSI_Static geometricrootpolynomic_dispersion();
			PSI_Static polynomicpowergeometric_dispersion();
			PSI_Static polynomicrootgeometric_dispersion();
			PSI_Static geometricpowergeometric_dispersion();
			PSI_Static geometricrootgeometric_dispersion();
			PSI_Static quadraticpowerpolynomic_dispersion();
			PSI_Static rootpowerpolynomic_dispersion();
			PSI_Static quadraticpowergeometric_dispersion();
			PSI_Static rootpowergeometric_dispersion();
			PSI_Static nexponential_dispersion();
			PSI_Static nlogarithmic_dispersion();
			PSI_Static bexponential_dispersion();
			PSI_Static blogarithmic_dispersion();
			PSI_Static exponential_dispersion();
			PSI_Static logarithmic_dispersion();
			PSI_Static strigonometric_dispersion();
			PSI_Static ctrigonometri_dispersion();
			PSI_Static rtrigonometric_dispersion();
			PSI_Static shiperbolic_dispersion();
			PSI_Static chiperbolic_dispersion();
			PSI_Static rhiperbolic_dispersion();
			PSI_Static selliptic_dispersion();
			PSI_Static celliptic_dispersion();
			PSI_Static relliptic_dispersion();
			PSI_Static sparabollic_dispersione();
			PSI_Static cparabollic_dispersion();
			PSI_Static rparabollic_dispersion();
			/*
				FORM MEASURES
					SYMMETRY
			*/
			PSI_Static fun();
			/* ****************************************************
			
				INFERENTIAL STATISTICS
					NORMALITY MEASURES
					MARGINALITY MEASURES
					EXCEPTION MEASURES
					ERROR MEASURES
					ANOMALY MEASURES
					SINGULARITY MEASURES
					HIPOTHESIS FALSIFICATION

			**************************************************** */ 
			/*
				NORMALITY MEASURES
			*/
			PSI_Static normality(int median, int dispersion, int form){

			};
			/*
				MARGINALITY MEASURES
			*/
			PSI_Static marginality(int normality, int limit){

			};
			/*
				EXCEPTION MEASURES
			*/
			PSI_Static exception(int normality, int marginality){

			};
			/*
				METANORMALITY MEASURES
			*/
			PSI_Static metanormality(int normalityMean, int normalityDispersion, int form ){

			};
			/*
				ANOMALY MEASURES
			*/
			PSI_Static anomaly(int metanormality, int limit);
			/*
				SINGULARITY MEASURES
			*/
			PSI_Static singularity(int metanormality, int anomaly);
			/*
				ERROR
			*/
			PSI_Static error(int exception);
			/*
				METAERROR
			*/
			PSI_Static metaerror(int singularity);
			/*
				ALTERANCE
			*/
			PSI_Static alterance(int exception);
			/*
				PARANOMY
			*/
			PSI_Static paranomy(int singularity);
			/*
				HIPOTHESIS FALSIFICATION
			*/
			PSI_Static deductive();
			PSI_Static inductive();
			PSI_Static abductive();
			PSI_Static probabilistic();
			/* OPERATORS */
			/* PRIVATE CONTAINER */
			private:
			/* PROTECTED CONTAINER */
			protected:
		};
	/* PROTECTED CONTAINER */
	protected:
	/* PUBLIC CONTAINER */
	public:
	/* CONSTRUCTOR */
		PSI_StatisticsSet();
		~PSI_StatisticsSet();
	/* ATTRIBUTES */
		template <typename T>
		static PSI_StatisticsDataSeries<T> DataSeries;
		template <typename T>
		static PSI_StatisticsDevice<T> StatisticsDevice;
	/* METHODS */

	/* OPERATORS */
};
	}
}
#endif