/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: complex.hpp
Description: Complex numbers library with basic
operators, transformations and algorihtms.
********************************************* */ 

/* *********************************************
DEFINES:
	COMPLEX
MACROS:

STRUCTS:
	icomplex_s
	fcomplex_s
	dcomplex_s
	complexf_s
ENUMS:

TYPES:
	icomplex_t
	fcomplex_t
	dcomplex_t
	complexf_t
CLASSES:
	Complex<T>
	ComplexFunction<T>
	ComplexFunctional<T>
METHODS:
	-Addition
	-Subtraction
	-Multiplication
	-Division
********************************************* */ 

#include "math.h"

#ifndef PSI_COMPLEX
#define PSI_COMPLEX

namespace PSI_MathUtility{
    namespace PSI_ArithmeticToolbox{
        namespace PSI_ComplexArithmeticsModule{
            static class PSI_ComplexArithmeticsClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_ComplexArithmeticsClassModule();
                ~PSI_ComplexArithmeticsClassModule();
                private:
                protected:
            };
        }
    }
}

template <typename T>
double arctan(int number);

typedef struct icomplex_s {
	int real;
	int imaginary;
	icomplex_t conj(icomplex_t in){
		icomplex_t out;
		out.real = in.real;
		out.imaginary = -in.imaginary;
		return out;
	};
	int mag(icomplex_t in){
		return ( pow<int>(in.real,2) + pow<int>(in.imaginary,2));
	};
	int ang(icomplex_t in){
		return arctan<int>(in.imaginary/in.real);
	};
} icomplex_t; 

typedef struct fcomplex_s {
	float real;
	float imaginary;
	fcomplex_t conj(fcomplex_t in){
		fcomplex_t out;
		out.real = in.real;
		out.imaginary = -in.imaginary;
		return out;
	};
	float mag(fcomplex_t in){
		return ( pow<float>(in.real,2) + pow<float>(in.imaginary,2));
	};
	float ang(fcomplex_t in){
		return arctan<float>(in.imaginary/in.real);
	};
} fcomplex_t; 

typedef struct dcomplex_s {
	double real;
	double imaginary;
	dcomplex_t conj(dcomplex_t in){
		dcomplex_t out;
		out.real = in.real;
		out.imaginary = -in.imaginary;
		return out;
	};
	double mag(dcomplex_t in){
		return ( pow<double>(in.real,2) + pow<double>(in.imaginary,2));
	};
	double ang(dcomplex_t in){
		return arctan<double>(in.imaginary/in.real);
	};
} dcomplex_t; 

typedef struct complexf_s{
	ComplexFunction<long double>  f;
	long double coefficient;
}complexf_t;


namespace PSI_MathUtility{
    namespace PSI_AlgebraToolbox{
        namespace PSI_ComplexArithmeticsModule{
static class PSI_ComplexArithmetics: public virtual PSI_MathUtilityModule{
    public:
    PSI_ComplexArithmetics();
    ~PSI_ComplexArithmetics();
    private:
    protected:
};
        }
    }
}


template <typename T>
class Complex {
	public:
	/* CONSTRUCTORES */
	Complex();
	~Complex();
	Complex();
	/* ATRIBUTOS */
	T real;
	T imaginary;

	/* MÉTODOS */	
	Complex conj(Complex in){
		Complex out;
		out.real = in.real;
		out.imaginary = -in.imaginary;
		return out;
	};
	T mag(Complex in){
		return ( pow<T>(in.real,2) + pow<T>(in.imaginary,2))
	};
	T ang(Complex in){
		return arctan<T>(in.imaginary/in.real);
	};
	private:
	protected:
};

template <typename T>
class ComplexFunction {
	public:
	/* CONSTRUCTORES */
	ComplexFunction();
	~ComplexFunction();
	ComplexFunction();
	/* ATRIBUTOS */
	T* Complex;
	/* MÉTODOS */	
	Complex* conj(Complex* in, int n){
		Complex* out;
		out = new Complex[n];
		for (int i; i ;i++){
			out.real[i] = in.real[i];
			out.imaginary[i] = -in.imaginary[i];
		}
		return out;
	};
	T* mag(Complex* in, int n){
		T* out;
		out = new T[n];
		for (int i; i ;i++){
			out[i] = ((in.real,2) + pow<T>(in.imaginary,2));
		}
		return out;
	};
	T* ang(Complex* in, int n){
		T* out;
		out = new T[n];
		for (int i; i ;i++){
			out[i] = (arctan<T>(in.imaginary/in.real););
		}
		return out;
	};
	private:
	protected:
};

class ComplexFunctional {
	public:
	/* CONSTRUCTORES */
	ComplexFunctional();
	~ComplexFunctional();
	ComplexFunctional();
	/* ATRIBUTOS */
	complexf_t* F;
	/* MÉTODOS */	
	private:
	protected:
};
#endif