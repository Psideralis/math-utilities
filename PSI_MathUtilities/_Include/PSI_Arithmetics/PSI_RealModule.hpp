/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: real.hpp
Description: Real numbers library with basic
operators, transformations and algorihtms.
********************************************* */ 

/* *********************************************
DEFINES:
	REAL
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
  	Real<T>
	RealFunction<T>
	RealFunctional<T>
METHODS:
	-Addition
	-Subtraction
	-Multiplication
	-Division
********************************************* */ 

#ifndef PSI_REAL
#define PSI_REAL


namespace PSI_MathUtility{
    namespace PSI_ArithmeticToolbox{
        namespace PSI_RealArithmeticsModule{
            static class PSI_RealArithmeticClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_RealArithmeticClassModule();
                ~PSI_RealArithmeticClassModule();
                private:
                protected:
            };
        }
    }
}


#endif