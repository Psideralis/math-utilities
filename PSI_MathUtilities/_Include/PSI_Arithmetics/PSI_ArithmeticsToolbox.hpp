/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_ArithmeticsToolbox.hpp
Description: Psideralis Arithmetics Toolkit

Arithmetics toolbox library for:
        -Operators
        -Conversion
        -Special Functions
        -Special Series
For natural, binary, module, elliptic, real and
complex arithmetics.
********************************************* */ 


#include "PSI_LogicToolbox.hpp"

#ifndef PSI_ARITHMETIC
#define PSI_ARITHMETIC

#include "PSI_BinaryModule.hpp"
#include "PSI_ComplexModule.hpp"
#include "PSI_ModuleArithmeticsModule.hpp"
#include "PSI_RealModule.hpp"

namespace PSI_MathUtility{
    namespace PSI_ArithmeticToolbox{
            static class PSI_Arithmetic: public virtual PSI_MathUtilityToolbox{

        }; 
    }
}

#endif