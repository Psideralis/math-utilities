/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: binary.hpp
Description: Binary numbers library with basic
operators, transformations and algorihtms.
********************************************* */ 

/* *********************************************
DEFINES:
	PSI_BINARY
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
  	Binary<T>
	BinaryArray<T>
METHODS:
	-Addition
	-Subtraction
	-Multiplication
	-Division
********************************************* */ 

#ifndef PSI_BINARY
#define PSI_BINARY

namespace PSI_MathUtility{
    namespace PSI_ArithmeticToolbox{
        namespace PSI_BinaryArithmeticsModule{
            static class PSI_BinaryArithmeticsClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_BinaryArithmeticsClassModule();
                ~PSI_BinaryArithmeticsClassModule();
                private:
                protected:
            };
        }
    }
}

#endif