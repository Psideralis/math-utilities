/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: module.hpp
Description: Module numbers library with basic
operators, transformations and algorihtms.
********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
  	Module<T>
	ModuleFunction<T>
	ModuleFunctional<T> 
METHODS:
	-Addition
	-Subtraction
	-Multiplication
	-Division
********************************************* */ 

#ifndef PSI_MODULEARITHMETICS
#define PSI_MODULEARITHMETICS



namespace PSI_MathUtility{
    namespace PSI_ArithmeticToolbox{
        namespace PSI_ModularArithmeticsModule{
            static class PSI_ModularArithmeticsClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_ModularArithmeticsClassModule();
                ~PSI_ModularArithmeticsClassModule();
                private:
                protected:
            };
        }
    }
}

#endif