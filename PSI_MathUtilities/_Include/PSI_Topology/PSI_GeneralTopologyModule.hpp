/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_GeneralTopologyModule.hpp
Description:
********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_GENERALTOPOLOGY
#define PSI_GENERALTOPOLOGY

namespace PSI_MathUtility
{
    namespace PSI_TopologyToolbox
    {
        namespace PSI_GeneralTopologyModule
        {
static class PSI_GeneralTopology: public virtual PSI_MathUtilityModule{
    public:
    PSI_GeneralTopology();
    ~PSI_GeneralTopology();
    private:
    protected:
}; 
        }   
    }
}

#endif