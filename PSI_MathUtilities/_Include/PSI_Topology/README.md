# Psideralis Math Utilities: Topology
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public - Progress 0%
## Version
00.00.000.001
## Description
Topology toolbox library includes:

        General
        Algebraic
            Linear
            Nonlinear
            Multilear  
            Nonmultilinear
        Geometric
            Topes
            Manifolds
        Analytical
            Differential
            Variational