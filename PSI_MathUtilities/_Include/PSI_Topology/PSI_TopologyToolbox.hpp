/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_TopologyToolbox.hpp
Description:
********************************************* */ 

#ifndef PSI_TOPOLOGY
#define PSI_TOPOLOGY

#ifndef CSTD
#define CSTD
    #include <iostream>
    #include <string>
    #include <fstream>
    #include <chrono>
    using namespace std;
    using namespace chrono;
#endif

namespace PSI_MathUtility
{
    namespace PSI_TopologyToolbox
    {
        static class PSI_Topology: public virtual PSI_MathUtilityToolbox{

        };
    }
}

#include "PSI_AlgebraicTopologyModule.hpp"
#include "PSI_AnalyticalTopologyModule.hpp"
#include "PSI_GeneralTopologyModule.hpp"

#endif