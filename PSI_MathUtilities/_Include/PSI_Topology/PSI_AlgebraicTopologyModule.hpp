/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_AlgebraicTopologyModule.hpp
Description:
********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_ALGEBRAICTOPOLOGY
#define PSI_ALGEBRAICTOPOLOGY

namespace PSI_MathUtility
{
    namespace PSI_TopologyToolbox
    {
        namespace PSI_AlgebraicTopologyModule
        {
static class PSI_AlgebraicTopology: public virtual PSI_MathUtilityModule{
    public:
    PSI_AlgebraicTopology();
    ~PSI_AlgebraicTopology();
    private:
    protected:
};
        }
    }
}

#endif