/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_AnalyticalTopology.hpp
Description:
********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_ANALYTICALTOPOLOGY
#define PSI_ANALYTICALTOPOLOGY

namespace PSI_MathUtility
{
    namespace PSI_TopologyToolbox
    {
        namespace PSI_AnalyticalTopologyModule
        {
static class PSI_AnalyticalTopology: public virtual PSI_MathUtilityModule{
    public:
    PSI_AnalyticalTopology();
    ~PSI_AnalyticalTopology();
    private:
    protected:
};
        }
    }
}

#endif