/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_ResolutoricModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:


CLASSES:

********************************************* */ 

#ifndef RESOLUTORIC
#define RESOLUTORIC

namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_ResolutoricModule
        {
            static class PSI_ResolutoricClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_ResolutoricClassModule();
                ~PSI_ResolutoricClassModule();
                private:
                protected:
            };
        }
    }
} 


#endif