/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_ProblematoricModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_PROBLEMATORIC
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_PROBLEMATORIC
#define PSI_PROBLEMATORIC


namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_ProblematoricModule
        {
            static class PSI_ProblematoricClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_ProblematoricClassModule();
                ~PSI_ProblematoricClassModule();
                private:
                protected:
            };
        }
    }
} 


#endif