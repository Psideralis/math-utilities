/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_FunctionalModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_FUNCTION
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
    PSI_Functional
NAMESPACE:
    PSI_MathUtility
    PSI_LogicToolbox
    PSI_FunctionalModule
FUNCTIONS:
********************************************* */ 

#ifndef PSI_FUNCTION
#define PSI_FUNCTION


namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_FunctionalModule
        {
            static class PSI_FunctionalClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_FunctionalClassModule();
                ~PSI_FunctionalClassModule();
                private:
                protected:
            };
        }
    }
} 


/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Function: public virtual Object{
    public:
    /* CONSTRUCTOR */
    PSI_Function(){}
     PSI_Function(string symbolic){}
    ~PSI_Function();
    PSI_Function(const PSI_Function &cpy);
    /* ATRIBUTOS */
    int domain;
    int codomain;
    int range;
    PSI_PointSeries<T>* x;
    PSI_PointSeries<T>* y;
    /* MÉTODOS */
    /* OPERADORES */
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Functional: public virtual Object{
    public:
    /* CONSTRUCTOR */
    PSI_Functional(){}
    PSI_Functional(string symbolic){}
    ~PSI_Functional();
    PSI_Functional(const PSI_Functional &cpy);
    /* ATRIBUTOS */
    PSI_Function<T>* function;
    /* MÉTODOS */
    /* OPERADORES */
    private:
    protected:
};

#endif