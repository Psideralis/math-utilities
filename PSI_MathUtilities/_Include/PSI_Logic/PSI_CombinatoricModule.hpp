/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_CombinatoricModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_COMBINATORIC
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
    PSI_Combinatoric
NAMESPACES:
    PSI_MathUtility
    PSI_LogicToolbox
    PSI_CombinatoricModule
FUNCTIONS:

********************************************* */ 

#ifndef PSI_COMBINATORIC
#define PSI_COMBINATORIC


namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_CombinatoricModule
        {
            static class PSI_CombinatoricClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_ProblematoricClassModule();
                ~PSI_ProblematoricClassModule();
                private:
                protected:
            };
        }
    }
} 


/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Combination: public virtual Object{
    public:
    PSI_Combination();
    ~PSI_Combination();
    PSI_Combination(const PSI_Combination &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_OrderedList: public virtual Object{
    public:
    PSI_OrderedList();
    ~PSI_OrderedList();
    PSI_OrderedList(const PSI_OrderedList &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Permutation: public virtual Object{
    public:
    PSI_Permutation();
    ~PSI_Permutation();
    PSI_Permutation(const PSI_Permutation &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};

#endif