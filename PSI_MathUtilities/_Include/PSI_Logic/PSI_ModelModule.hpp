/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_ModelModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_MODELS
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_MODEL
#define PSI_MODEL


namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_ModelModule
        {
            static class PSI_ModelClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_ModelClassModule();
                ~PSI_ModelClassModule();
                private:
                protected:
            };
        }
    }
} 


#endif