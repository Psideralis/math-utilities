#include "PSI_LogicToolbox.hpp"

using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_CombinatoricModule;
using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_FunctionalModule;
using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_GraphModule;
using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_ModelModule;
using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_ProblematoricModule;
using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_ResolutoricModule;
using namespace PSI_MathUtility::PSI_LogicToolbox::PSI_SetModule;

int main(){
    // LOGIC
    PSI_LogicString         myLogicString;
    PSI_TruthTable          myTruthTable;
    PSI_LogicDevice         myLogicDevice;
    PSI_AnalyticalDevice    myDevice;
    PSI_SyntheticDevice     mySyntheticDevice;
    PSI_Predicate           myPredicate;
    PSI_ComplexPredicate    myComplexPredicate;
    PSI_Proposition         myProposition;
    PSI_ComplexProposition  myComplexProposition;

    // SET
    PSI_Set<int>            mySet;

    // COMBINATORICS
    PSI_Combination<int>    myCombination;
    PSI_OrderedList<int>    myOrderedList;
    PSI_Permutation<int>    myPermutation;
    
    // FUNCTIONAL
    PSI_Function<int>       myFunction;
    PSI_Functional<int>     myFunctional;

    // MODEL

    // PROBLEMATORIC


    // RESOLUTORIC

    // GRAPH
    PSI_Graph<int>          myGraph;
    PSI_Tree<int>           myTree;
    return 0;
}