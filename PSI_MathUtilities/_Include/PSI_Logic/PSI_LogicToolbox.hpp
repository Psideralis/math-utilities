/* *********************************************
Author: PsPSI_IDeralis
License: GNU GPL 3.0
File name: PSI_LogicToolbox.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_LOGIC
    DECLARE_EXPORT
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
    PSI_Logic
NAMESPACES:
    PSI_MathUtility
    PSI_LogicToolbox
FUNCTIONS:
********************************************* */ 

#ifndef PSI_LOGIC
#define PSI_LOGIC

#define DECLARE_EXPORT

#ifdef DECLARE_EXPORT
#    define EXPORT_API __declspec(dllexport)
#else
#    define EXPORT_API __declspec(dllimport)
#endif

#ifndef STDC
#define STDC
    #include "stdlib.h"
    #include "stdio.h"
    #include "string"
    using namespace std;
#endif

#ifndef CPPSTD
#define CPPSTD
    #include <iostream>
    #include <string>
    #include <fstream>
    #include <chrono>
    using namespace std;
    using namespace chrono;
#endif

#include "PSI_CombinatoricModule.hpp"
#include "PSI_FunctionalModule.hpp"
#include "PSI_GraphModule.hpp"
#include "PSI_ModelModule.hpp"
#include "PSI_SetModule.hpp"
#include "PSI_ProblematoricModule.hpp"
#include "PSI_ResolutoricModule.hpp"

namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        static class PSI_LogicToolbox: public virtual PSI_MathUtilityToolbox{
                public:
                PSI_LogicToolbox();
                ~PSI_LogicToolbox();
                private:
                protected:
        }; 
    }
}


/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
class EXPORT_API PSI_Object{
    public:
    PSI_Object();
    ~PSI_Object();
    PSI_Object(const PSI_Object &cpy);
    void set_Type();
    void get_Type();
    // METHODS    
    private:
    // ATRIBUTES
        PSI_Type* PSI_Token;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
class EXPORT_API PSI_Type: public virtual PSI_Object{
    public:
    PSI_Type();
    ~PSI_Type();
    PSI_Type(const PSI_Type &cpy);
    // ATRIBUTES
    ONTIC_N0        PSI_OnticType;
    EPISTEMIC_N0    PSI_EpistemicType; 
    LOGIC_N0        PSI_LogicType;
    LINGUISTIC_N0   PSI_LinguisticType;
    MATHEMATICAL_N0 PSI_MathematicalType;
    // METHODS   
    private:
    PSI_Type* PSI_SuperToken;
    PSI_Type* PSI_SubToken;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
class EXPORT_API PSI_MathUtilityToolbox :  public virtual PSI_Object{
    public:
    PSI_MathUtilityToolbox();
    ~PSI_MathUtilityToolbox();
    PSI_MathUtilityToolbox(const PSI_MathUtilityToolbox &cpy);
    // ATRIBUTES
    void set_ID();
    void get_ID();
    void set_Tag();
    void get_Tag();
    void set_Version();
    void get_Version();
    void set_Author();
    void get_Author();
    void set_Doc();
    void get_Doc();
    void set_Wiki();
    void get_Wiki();
    // METHODS    
    private:
        string PSI_ID;
        string PSI_Tag;
        string PSI_Version;
        string PSI_Author;
        string PSI_Doc;
        string PSI_Wiki;
        PSI_Type* PSI_Class;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
class EXPORT_API PSI_MathUtilityModule: virtual public PSI_MathUtilityToolbox,  public virtual PSI_Object{
    public:
    PSI_MathUtilityModule();
    ~PSI_MathUtilityModule();
    PSI_MathUtilityModule(const PSI_MathUtilityModule &cpy);
    // ATRIBUTES

    // METHODS    
    private:
    protected:
};

/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
class EXPORT_API PSI_LogicString: public virtual PSI_Object{
    public:
    PSI_LogicString();
    ~PSI_LogicString();
    PSI_LogicString(const PSI_LogicString &cpy);
    // ATRIBUTES

    // METHODS    
    private:
        string value;
        string* tokens;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
class EXPORT_API PSI_Predicate: public virtual PSI_Object{
    public:
    PSI_Predicate();
    ~PSI_Predicate();
    PSI_Predicate(const PSI_Predicate &cpy);
    // ATRIBUTES

    // METHODS    
    private:
        PSI_LogicString predicate;
        bool truthValue;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:


TODO:
    //
********************************************* */ 
class EXPORT_API PSI_ComplexPredicate: public virtual PSI_Object{
    public:
    PSI_ComplexPredicate();
    ~PSI_ComplexPredicate();
    PSI_ComplexPredicate(const PSI_ComplexPredicate &cpy);
    // ATRIBUTES

    // METHODS
    private:
        PSI_LogicString complexPredicate;
        bool truthValue;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:


TODO:
    //
********************************************* */ 
class EXPORT_API PSI_Proposition: public virtual PSI_Object{
    public:
    PSI_Proposition();
    ~PSI_Proposition();
    PSI_Proposition(const PSI_Proposition &cpy);
    // ATRIBUTES

    // METHODS    
    private:
        PSI_LogicString proposition;
        bool truthValue;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:


TODO:
    //
********************************************* */ 
class EXPORT_API PSI_ComplexProposition: public virtual PSI_Object{
    public:
    PSI_ComplexProposition();
    ~PSI_ComplexProposition();
    PSI_ComplexProposition(const PSI_ComplexProposition &cpy);
    // ATRIBUTES

    // METHODS
    private:
        PSI_LogicString complexProposition;
        bool truthValue;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    //Computes truth value
********************************************* */ 
class EXPORT_API PSI_TruthTable: public virtual PSI_Object{
    public:
    PSI_TruthTable();
    ~PSI_TruthTable();
    PSI_TruthTable(const PSI_TruthTable &cpy);
    // ATRIBUTES

    // METHODS    
    private:
        int* PSI_truthTable_Matrix;
        PSI_LogicString complexProposition;
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:


TODO:
    // Theorem completition & discovery
    // Demostration and proofs
    // Axiomatic Set Theory
    // Axiomatic Arithmetic Theory
    // Axiomatic Algebra Theory
    // Axiomatic Geometry Theory
    // Axiomatic Formal Theory
********************************************* */ 
class EXPORT_API PSI_LogicDevice: public virtual PSI_Object{
    public:
    PSI_LogicDevice(){};
    ~PSI_LogicDevice();
    PSI_LogicDevice(const PSI_LogicDevice &cpy);
    // ATRIBUTES
    // METHODS  
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    // Conjectures & hypothesis
    // Logic function modelling
    // Analytic Mechanic Axiomatic Theory
    // Axiomatic Material Theory
********************************************* */ 
class EXPORT_API PSI_AnalyticalDevice: public virtual PSI_Object{
    public:
    PSI_AnalyticalDevice(){};
    ~PSI_AnalyticalDevice();
    PSI_AnalyticalDevice(const PSI_AnalyticalDevice &cpy);
    // ATRIBUTES
    // METHODS  
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    // Knowledge discovery and representation
    // Crystalography Axiomatic Theory
    // Axiomatic Synthetic Theory
********************************************* */ 
class EXPORT_API PSI_SyntheticDevice: public virtual PSI_Object{
    public:
    PSI_SyntheticDevice(){};
    ~PSI_SyntheticDevice();
    PSI_SyntheticDevice(const PSI_SyntheticDevice &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};

#endif