/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_SetModule.hpp
Description: Set of numbers utilities. 

For set of objects see tuple on 
    PSI_DataStructures&Algorithms/Static.
********************************************* */ 

/* *********************************************
DEFINES:
    PSI_SET
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:
    PSI_SetModule
NAMESPACES: 
    PSI_MathUtility
    PSI_LogicToolbox
    PSI_SetModule
FUNCTIONS:

********************************************* */ 


#ifndef PSI_SET
#define PSI_SET

namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_SetModule
        {
            static class PSI_SetClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_SetClassModule();
                ~PSI_SetClassModule();
                private:
                protected:
            };
        }
    }
} 

/*
	Entrophy of a set: Space of Sets
	
		- Get the domain and type: [inf,sup], {N,Z,Q,R,C}
		- Get the number of available elements: m = ( sup-inf )*|resolution|
		- Get the number of choosen elements or range cardinality: ( n < m or  n = m ), n ∈ N
		    - Determine if there are repeated elements  sum(p(n): total of repeated elements).
	    	- Determine if the elements are ordered     sum(o(n): number of ordered elements, ex. <,>,>=,<= ).

        Entrophy calculation:
            - Stable enthophy: e_0 = 1, n = m
			- If not ordered nor repeated: e_ref = m/n > 1
			- If not ordered but repeated: e_rep = m/n + ((sum(p(n))+n)/n-1),  a >= 0
			- If ordered but no repeated:  e_ord = m/n - ((sum(o(n))+n)/n-1),  b = , 0 [resolution] < b < 1 [resolution]
			- If ordered and repeated:     e_prd = m/n + ((sum(p(n))+n)/n-1) - (sum(o(n))+n)/n

        e < 1   Low Enthropy ---> low heat (cold set point).
        e = 1   Enthropy equilibrium between domain and range.
        e > 1   High enthropy --> high heat (hot set poing).
        
	The domain with resolution, a range cardinality and the entrophy determines a set point heat. A set point heat descriptions is a set of markers
	that expose a particular set of sets which are require for a specific application represente by a and b.
        
        a: Repetition cardinality --> Reduce enthropy.      0 <= inf < a < sup
        b. Ordination cardinality --> Augment enthropy.     0 <  inf < b < sup < 1

    Ex:
        Domain:             R [10,100] = 90*1 000 000 000 = 90 000 000 000
        Range cardinality:  90 000 000 000
        A. Enthropy:           1.5 {a = , b = }

        B .Enthropy:           0.75
        
        Diferential Enthopy:
            A   = +0.5
            B   = -0.25
            A-B = +0.25
    Ex:
        Domain:             R [10,100] = 90*1 000 000 000 = 90 000 000 000
        Range cardinality:  90 000 {a = , b = }
        A. Enthropy:           1 000 000

        B. Enthropy:  

        Diferential Enthopy: 
            A   = 
            B   = 
            A-B =         
    Ex:
        Domain:             R [10,100] = 90*1 000 000 000 = 90 000 000 000
        Range cardinality:  900 000 000 000 {a = , b = }
        Range cardinality:  
        A. Enthropy:          

        B. Enthropy:   

        Diferential Enthopy:
            A   = 
            B   = 
            A-B =      

*/

/* *********************************************
CLASS:
	NAME:
	DESCRIPTION: a set of numbers.
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    // Create set
    // Ordered set
    // Repeated set
    // Selected set
        // Combinations
        // Permutations
        // Ordered lists
    // Operations
        - Union
        - Exclusive Union
        - Intersection
        - Complement
        - Ordered Set
        - Power Set
        - Boolean Set
********************************************* */ 
template <typename T>
class PSI_Set: public virtual Object{
    public:
    // CONSTRUCTORS
    PSI_Set(){};
    PSI_Set(int domain, int rangeCardinality, int enthropy);
    PSI_Set(int domain, int rangeCardinality, int enthropy, int a, int b);
    PSI_Set(int, ...);
    PSI_Set(PSI_Set<T> set);
    PSI_Set(PSI_Set<T>, ...);
    ~PSI_Set();
    PSI_Set(const PSI_Set<T> &cpy);
    // ATRIBUTES

    // METHODS
    void get_entry();
    void get_size();
    void get_depth();
    void get_domain();
    void get_domainInterval();
    void get_domainResolution();
    void get_range();
    void get_rangeInterval();
    void get_rangeCardinality();
    void set_entry();
    void set_size();
    void set_domain();
    void set_depth();
    void set_domainInterval();
    void set_domainResolution();
    void set_range();
    void set_rangeInterval();
    void set_rangeCardinality();
    private:  
        T* entry;
        int size;
        int depth;
        char domain;
        int[2] domainInterval;
        int domainResolution;
        char range;
        int[2] rangeInterval;
        int rangeCardinality;  
    protected:

};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION: An ordered set by an ordered
        algebraic law.
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    // Create set
    // Reordered set
    // Repeated set
    // Selected set
        // Combinations
        // Permutations
        // Ordered lists
    // Operations
        - Union
        - Exclusive Union
        - Intersection
        - Complement
        - Ordered Set
        - Power Set
        - Boolean Set
********************************************* */ 
template <typename T>
class OrderedSet: public virtual Object{
    public:
    // CONSTRUCTORS
    OrderedSet(){};
    OrderedSet(PSI_Set<T> set);
    OrderedSet(PSI_Set<T>,...);
    ~OrderedSet();
    OrderedSet(const OrderedSet<T> &cpy);
    // ATRIBUTES
    // METHODS
    void get_entry();
    void get_size();
    void get_depth();
    void get_domain();
    void get_domainInterval();
    void get_domainResolution();
    void get_range();
    void get_rangeInterval();
    void get_rangeCardinality();
    void set_entry();
    void set_size();
    void set_domain();
    void set_depth();
    void set_domainInterval();
    void set_domainResolution();
    void set_range();
    void set_rangeInterval();
    void set_rangeCardinality();
    private:
        T* entry;
        int size;
        int depth; 
        int[2] domainInterval;
        int domainResolution;
        char range;
        int[2] rangeInterval;
        int rangeCardinality;   
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION: The set of all sets a set.
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    // Create set
    // Ordered set
    // Repeated set
    // Selected set
        // Combinations
        // Permutations
        // Ordered lists
    // Operations
        - Union
        - Exclusive Union
        - Intersection
        - Complement
        - Ordered Set
        - Power Set
        - Boolean Set
********************************************* */ 
template <typename T>
class PowerSet: public virtual Object{
    public:
    // CONSTRUCTORS
    PowerSet();
    PowerSet(PSI_Set<T> set);
    PowerSet(PSI_Set<T>,...);
    PowerSet(const PowerSet<T> &cpy);
    // ATRIBUTES
    // METHODS
    void get_entry();
    void get_size();
    void get_depth();
    void get_domain();
    void get_domainInterval();
    void get_domainResolution();
    void get_range();
    void get_rangeInterval();
    void get_rangeCardinality();
    void set_entry();
    void set_size();
    void set_domain();
    void set_depth();
    void set_domainInterval();
    void set_domainResolution();
    void set_range();
    void set_rangeInterval();
    void set_rangeCardinality();
    private:
        T* entry;
        int size;
        int depth;  
        int[2] domainInterval;
        int domainResolution;
        char range;
        int[2] rangeInterval;
        int rangeCardinality;  
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION: Assign to every set element
        the value true or false.
	ATRIBUTES:
	METHODS:
	OPERATORS:

TODO:
    // Create set
    // Ordered set
    // Repeated set
    // Selected set
        // Combinations
        // Permutations
        // Ordered lists
    // Operations
        - Union
        - Exclusive Union
        - Intersection
        - Complement
        - Ordered Set
        - Power Set
        - Boolean Set
********************************************* */ 
template <typename T>
class BooleanSet: public virtual Object{
    public:
    // CONSTRUCTORS
    BooleanSet();
    BooleanSet(PSI_Set<T> set);
    BooleanSet(PSI_Set<T>,...);
    ~BooleanSet();
    BooleanSet(const BooleanSet<T> &cpy);
    // ATRIBUTES
    // METHODS
    void get_entry();
    void get_size();
    void get_depth();
    void get_domain();
    void get_domainInterval();
    void get_domainResolution();
    void get_range();
    void get_rangeInterval();
    void get_rangeCardinality();
    void set_entry();
    void set_size();
    void set_domain();
    void set_depth();
    void set_domainInterval();
    void set_domainResolution();
    void set_range();
    void set_rangeInterval();
    void set_rangeCardinality();
    private:
        T* entry;
        int size; 
        int depth; 
        int[2] domainInterval;
        int domainResolution;
        char range;
        int[2] rangeInterval;
        int rangeCardinality; 
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class SpaceSeries: public virtual Object {
    public:
    // CONSTRUCTORS
    SpaceSeries();
    ~SpaceSeries();
    SpaceSeries(const SpaceSeries<T> &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class TimeSeries : public virtual Object{
    public:
    // CONSTRUCTORS
    TimeSeries();
    ~TimeSeries();
    TimeSeries(const TimeSeries<T> &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class EventSeries: public virtual Object {
    public:
    // CONSTRUCTORS
    EventSeries();
    ~EventSeries();
    EventSeries(const EventSeries<T> &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PointSeries: public virtual Object{
    public:
    // CONSTRUCTORS
    PointSeries();
    ~PointSeries();
    PointSeries(const PointSeries<T> &cpy);
    // ATRIBUTES
    // METHODS
    private:
    protected:
};

#endif