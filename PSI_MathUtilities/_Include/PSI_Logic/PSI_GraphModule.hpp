/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_GraphModule.hpp
Description:

********************************************* */ 

/* *********************************************
DEFINES:
    PSI_GRAPH
MACROS:

STRUCTS:

ENUMS:

TYPES:

NAMESPACES:
    PSI_MathUtility
    PSI_LogicToolbox
    PSI_GraphsModule
CLASSES:

********************************************* */ 

#ifndef PSI_GRAPH
#define PSI_GRAPH


namespace PSI_MathUtility
{
    namespace PSI_LogicToolbox
    {
        namespace PSI_GraphModule
        {
            static class PSI_GraphClassModule: public virtual PSI_MathUtilityModule{
                public:
                PSI_GraphClassModule(){};
                ~PSI_GraphClassModule();
                private:
                protected:
            };
        }
    }
} 

/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Node: public virtual Object{
    public:
    PSI_Node();
    ~PSI_Node();
    PSI_Node(const PSI_Node &cpy);
    // ATRIBUTES
    // METHODS    
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_BinaryNode: public virtual Object{
    public:
    PSI_BinaryNode();
    ~PSI_BinaryNode();
    PSI_BinaryNode(const PSI_BinaryNode &cpy);
    // ATRIBUTES
    PSI_BinaryNode<T> rhs;
    PSI_BinaryNode<T> lhs;
    // METHODS    
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Tree: public virtual Object{
    public:
    PSI_Tree();
    ~PSI_Tree();
    PSI_Tree(const Tree &cpy);
    // ATRIBUTES
    PSI_BinaryNode<T> root;
    // METHODS    
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Graph: public virtual Object{
    public:
    PSI_Graph();
    ~PSI_Graph();
    PSI_Graph(const PSI_Graph &cpy);
    // PSI_Node
    Node<T> root;
    // METHODS    
    private:
    protected:
};
/* *********************************************
CLASS:
	NAME:
	DESCRIPTION:
	ATRIBUTES:
	METHODS:
	OPERATORS:
********************************************* */ 
template <typename T>
class PSI_Graphor: public virtual Object{
    public:
    PSI_Graphor();
    ~PSI_Graphor();
    PSI_Graphor(const PSI_Graphor &cpy);
    // ATRIBUTES
    PSI_Node<PSI_Tensor> root;
    // METHODS    
    private:
    protected:
};
#endif