# Psideralis Math Utilities:  Numerical analysis
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public - Progress 0%
## Version
00.00.000.001
## Description
Numerical analysis toolbox library includes:

    Derivatives
        Ordinaries
        Partial
        Vectorial
        Tensorial
    Integrals
        Scalar
        Vectorial
        Tensorial
    Analytical Algebra
        Differential
            Scalar
            Vectorial
            Tensorial
        Variational
            Scalar
            Vectorial
            Tensorial
    Differential equations
        Ordinary
        Partial
    Variational equations
        Sup/Inf
        Min/Max