/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: math.hpp
Description:
********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

FUNCTIONS:
    sqr
    sqr_root
    pow
    pol
    pluri_pol
    rad
    rat
    smm
    pdt
    fac
    sin
    arcsin
    sinh
    arcsinh
    sine
    arcsine
    cos
    arcos
    cosh
    arcosh
    cose
    arcose
    tan
    arctan
    tanh
    arctanh
    tane
    arctane
    sec
    arcsec
    sech
    arcsech
    sece
    arcsee
    csc
    arccsc
    csch
    arccsch
    csce
    arccsce
    cot
    arccot
    coth
    arccoth
    cote
    arccote
    exp
    log
    ln
TYPES:

CLASSES:

********************************************* */ 
#ifndef PSI_MATH
#define PSI_MATH


namespace PSI_MathUtility
{
    namespace PSI_NumericalAnalysisToolbox
    {
        namespace PSI_MathAnalysisModule
        {
static class PSI_MathAnalysis: public virtual PSI_MathUtilityModule{
    public:
    PSI_MathAnalysis();
    ~PSI_MathAnalysis();
    private:
    protected:
};
/*
Name: Square Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T sqr(T x, T n){
    T out = x;
    for (int i = 1; i <= n; i++){
       out =* x;
    }
    return out;
};
/*
Name: Square Root Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T sqr_root(T x, T n){
    T out = x;
    for (int i = 1; i <= n; i++){
       out =* x;
    }
    return out;
};

/*
Name: Power Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T pow(T x, T n){
    T out = x;
    for (int i = 1; i <= n; i++){
       out =* x;
    }
    return out;
};

/*
Name: Polinomial Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T pol(T x, int order, T* vector ,double long res);

/*
Name: Pluripolinomial Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T pluri_pol(T x, int order, T* vector ,double long res);

/*
Name: Radical Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T rad(T x, int order, T* vector ,double long res);

/*
Name: Rational Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T rat(T x, int order, T* vector ,double long res);

/*
Name: Factorial Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T fac(T n){
    T out = n;
    for (int i = n-1; i > 0 ; i--){
       out = out*(i);
    }
    return out;
};

/*
Name: Summatory Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T smm(T n){
    T out = n;
    for (int i = n-1; i > 0 ; i--){
       out = out*(i);
    }
    return out;
};

/*
Name: Productory Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T pdt(T n){
    T out = n;
    for (int i = n-1; i > 0 ; i--){
       out = out*(i);
    }
    return out;
};

/*
Name: Sine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T sin(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ (((pow(-1,i))/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Arcsine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arcsin(T x);

/*
Name: Hiperbolic Sine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T sinh(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Archiperbolic Sine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arcsinh(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Elliptic Sine Sine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T sine(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Arcelliptic Sine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arcsine(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};


/*
Name: Cosine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T cos(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ (((pow(-1,i))/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Arcosine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arccos(T x);

/*
Name: Hiperbolic Cosine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T cosh(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Archiperbolic Cosine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/

template <typename T>
T arccosh(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Elliptic Cosine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/

template <typename T>
T cose(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Arcelliptic Cosine Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/

template <typename T>
T arccose(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Tangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T tan(T x, double long res);

/*
Name: Arctangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arctan(T x);

/*
Name: Hiperbolic Tangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T tanh(T x, double long res);

/*
Name: Archiperbolic Tangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T tanh(T x, double long res);

/*
Name: Elliptic Tangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T tane(T x, double long res);

/*
Name: Arcelliptic Tangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arctane(T x, double long res);

/*
Name: Secant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T sec(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ (((pow(-1,i))/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Arcsecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arcsec(T x);

/*
Name: Hiperbolic Secant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T sech(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Archiperbolic Secant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arcsech(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Elliptic Secant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T sece(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};

/*
Name: Arcelliptic Secant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arcsece(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac((2*i)+1)))*(pow(x,((2*i)+1)))); 
    }
    return out;
};


/*
Name: Cosecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
static T csc(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ (((pow(-1,i))/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Arcosecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arccsc(T x);

/*
Name: Hiperbolic Cosecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T csch(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Archiperbolic Cosecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/

template <typename T>
T arccsch(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Elliptic Cosecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/

template <typename T>
T csce(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Arcelliptic Cosecant Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/

template <typename T>
T arccsce(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1)/(fac(2*i)))*(pow(x,(2*i)))); 
    }
    return out;
};

/*
Name: Cotangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T cot(T x, double long res);

/*
Name: Arccotangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arccot(T x);

/*
Name: Hiperbolic Cotangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T coth(T x, double long res);

/*
Name: Archiperbolic Cotangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arccoth(T x, double long res);

/*
Name: Elliptic Cotangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T cote(T x, double long res);

/*
Name: Arcelliptic Cotangent Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T arccote(T x, double long res);

/*
Name: Exponential Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T exp(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((pow(x,i))/(fac(i))); 
    }
    return out;
};

/*
Name: Logarithmic Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T log(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1/((2*i)+1))*(pow(((x-1)/(x+1)),((2*i)+1)))); 
    }
    return 2*out;
};

/*
Name: Natural Logarithmic Function
Description:
Input:
    T x
    T n
Output:
    T result
Example:
*/
template <typename T>
T ln(T x, double long res){
    T out = 0;
    for (int i = 1; i < res; i++){
       out =+ ((1/((2*i)+1))*(pow(((x-1)/(x+1)),((2*i)+1)))); 
    }
    return 2*out;
};        
        }
    }
}

#endif