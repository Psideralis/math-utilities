/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_VariationalModule.hpp
Description:
********************************************* */ 

#ifndef PSI_VARIATIONAL
#define PSI_VARIATIONAL

namespace PSI_MathUtility
{
    namespace PSI_NumericalAnalysisToolbox
    {
        namespace PSI_VariationalAnalysisModule
        {
static class PSI_VariationalAnalysis: public virtual PSI_MathUtilityModule{
    public:
    PSI_VariationalAnalysis();
    ~PSI_VariationalAnalysis();
    private:
    protected:
};
        }
    }
}

#endif