/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: numerical.hpp
Description:
********************************************* */ 

/* *********************************************
DEFINES:

MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#include "PSI_MathModule.hpp"
#include "PSI_VariationalModule.hpp"


#ifndef PSI_NUMERICALANALYSIS
#define PSI_NUMERICALANALYSIS


namespace PSI_MathUtility
{
    namespace PSI_NumericalAnalysisToolbox
    {
		static class PSI_NumericalAnalysis: public virtual PSI_MathUtilityToolbox{

        };  
    }
}

template <typename T>
class MathNumerical{
    public:
    /* CONSTRUCTORES */
    
    /* ATRIBUTOS */

    /* MÉTODOS */
    
    /*
    Name: Power Function
    Description:
    Input:
    Output:
    Example:
    */
    static T* time(T jmp, T end){
        T* time;
        T j = 0;
        time = new T[jmp*end];
        for(int i = 0; i < (jmp*end) ; i++){
            time[i] = j;
            j = j + jmp;
        }
        return time;
    };
    /*
    Name: Power Function
    Description:
    Input:
    Output:
    Example:
    */
    static T integrate(T value){

    };
    /*
    Name: Power Function
    Description:
    Input:
    Output:
    Example:
    */
    static T derivate(T value){

    };    
    /* OPERADORES */

    private:
    
    protected:
};

#endif