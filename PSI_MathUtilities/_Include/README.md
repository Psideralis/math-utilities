# Psideralis Math Utilities:  Resume
## Author
Psideralis
## License
Psideralis Public Commune License
## Status
Public - Progress 0%
## Version
00.00.000.001
## Description
Logic toolbox library includes:

# Logic
    Logic Devices
    Sets
    Combinatorics
    Graphs
    Models
    Problems
    Resolutions
    Expert Systems
# Arithmetics
    Numeric domains
    Numeric systems
    Big number arithmetics
    Numeric series
    Numeric composition
# Geometry
    Coordinates
    Standard geometry
    NonStandard geometry
    Geometrical algebra
    Geometrical analysis
    Geometrical combinatorics
# Algebra
    Symbolic systems
    Abstract algebra
    Computational agebra
    Universal algebra
    Categories
# Numerical Analysis
    Derivatives
    Integrals
    Analytical algebra
    Differential equations
    Variational equations
# Topology
    General
    Algebraic
    Geometric
    Analytical
# Statistics
	Descriptive statistics
	Inferential statistics
	Hypothesis falsification
	Probability
# Optimization
    Algebraic    
    Geometrical
    Combinatorial
    Analytical
# Machine Learning
    Predictive Statistics: Supervised Learning
    Analytic Statistics: Semisupervised Learning
    Optimization Statistics: Reinforced Learning
    Metamodelling Statistics: Unsupervised Learning
# Control
    Signals
    Frequency & Time Domain
    Circuit System Methods
    State Space Methods
    State Machine Methods