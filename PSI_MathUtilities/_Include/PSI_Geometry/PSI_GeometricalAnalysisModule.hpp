/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_GeometricalAnalysisModule.hpp
Description: 
********************************************* */ 


/* *********************************************
DEFINES:
    PSI_GEOANALYSIS
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_GEOANALYSIS
#define PSI_GEOANALYSIS

namespace PSI_MathUtility
{
    namespace PSI_GeometryToolbox
    {
        namespace PSI_GeometricalAnalysisModule
        {
static class PSI_GeometricalAnalysis: public virtual PSI_MathUtilityModule{
    public:
    PSI_GeometricalAnalysis();
    ~PSI_GeometricalAnalysis();
    private:
    protected:
};
        }
    }
} 

#endif