/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_CoordinatesModule.hpp
Description: Cartesian coordinates points and
point collections. Includes basic operator over
points and points collections.
********************************************* */ 

/* *********************************************
DEFINES:
	PSI_COORDINATES
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

METHODS:

********************************************* */ 

#ifndef PSI_COORDINATES
#define PSI_COORDINATES

namespace PSI_MathUtility
{
    namespace PSI_GeometryToolbox
    {
		namespace PSI_CoordinatesModule
        {
static class PSI_Coordinates: public virtual PSI_MathUtilityModule{
    public:
    PSI_Coordinates();
    ~PSI_Coordinates();
    private:
    protected:
};
        }
        /* *********************************************
CLASS:

NAME:

DESCRIPTION:

ATRIBUTES:

METHODS:

OPERATORS:

********************************************* */ 
template <typename T>
class CPoint2D{
	public:
	/* CONSTRUCTORS */
	CPoint2D(){};
	CPoint2D(T x, T y){
		this->setX(x);
		this->setY(y);
	};
	~CPoint2D(){
		this->setX(0);
		this->setY(0);
	};
	CPoint2D(const CPoint2D &cpy){
		this->setX(cpy.x);
		this->setY(cpy.y);
	}
	/* PROPERTIES */
	T x;
	T y;
	/* METHODS */
	T getX(){
		return this->x;
	};
	T getY(){
		return this->y;
	};
	void setX(T x){
		this->x=x;
	};
	void setY(T y){
		this->y=y;
	};
	SPoint2D<T> SphericalTransform(){
		SPoint2D<T> sphere;
		sphere.r = sqr<T>(pow<T>(this->,2)+pow<T>(this.>y,2));
		sphere.theta = arctan(this.>y/this.>x)
		return sphere;
	}
	static SPoint2D<T> SphericalTransform(CPoint2D<T> point){
		SPoint2D<T> sphere;
		sphere.r = sqr<T>(pow<T>(point.x,2)+pow<T>(point.y,2));
		sphere.theta = arctan(point.y/point.x)
		return sphere;
	}
	CPoint2D<T> Traslation(T x, T y){
		CPoint2D<T> point;
		this->x += x;
		this->y += y; 
		return this;
	}
	SPoint2D<T> Rotation(T theta){
		this->x += sqr<T>(pow<T>(this->x,2)+pow<T>(this->y,2))*cos<T>(theta);
		this->y += sqr<T>(pow<T>(this->x,2)+pow<T>(this->y,2))*sin<T>(theta); 
		return this;
	}
	SPoint2D<T> XInversion(){
		this->x += -x;
		return this;
	}
	SPoint2D<T> YInversion(){
		this->y += -y; 
		return this;
	}
	SPoint2D<T> XYInversion(){
		CPoint2D<T> point;
		this->x += -x;
		this->y += -y; 
		return this;
	}
	/* OPERATORS */
	CPoint2D<T>& operator==(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator==(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator=(const CPoint2D<T> &rhs){
		
	};
	CPoint2D<T>& operator=(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator+=(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator+=(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator+(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator+(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator-=(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator-=(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator-(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator-(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator*=(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator*=(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator*(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator*(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator/=(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator/=(const CPoint2D<T>* &rhs){

	};
	CPoint2D<T>& operator/(const CPoint2D<T> &rhs){

	};
	CPoint2D<T>& operator/(const CPoint2D<T>* &rhs){
		
	};
	private:
	protected:
};

    }
} 


#endif