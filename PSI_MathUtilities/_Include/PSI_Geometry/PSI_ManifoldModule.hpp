/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_ManifoldModule.hpp
Description: Euclidean and non euclidean
phase of polytopes and generalized manifold 
classes, operators and transformations.
********************************************* */ 

/* *********************************************
DEFINES:
    PSI_MANIFOLD
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_MANIFOLD
#define PSI_MANIFOLD

namespace PSI_MathUtility
{
    namespace PSI_GeometryToolbox
    {
        namespace PSI_ManifoldModule
        {
static class PSI_Manifold: public virtual PSI_MathUtilityModule{
    public:
    PSI_Manifold();
    ~PSI_Manifold();
    private:
    protected:
};
        }
    }
} 

#endif