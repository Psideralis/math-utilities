/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_TopesModule.hpp
Description: Elementary topes and basic topes
operators and transformations.
********************************************* */ 

/* *********************************************
DEFINES:
	TOPES
MACROS:

STRUCTS:
	///////////////////////// ELEMENTS
	PSI_Point2D_s
	PSI_Point3D_s
	PSI_Point4D_s
	PSI_PointnD_s
	PSI_Line2D_s
	PSI_Line3D_s
	PSI_Line4D_s
	PSI_LineND_s
	PSI_Curve2D_s
	PSI_Curve3D_s
	PSI_Curve4D_s
	PSI_CurveND_s
	PSI_Angle_s
	PSI_ArcAngle_s
	PSI_SolidAngle_s
	PSI_Plane_s
	PSI_Surface_s
	PSI_Volume_s
	PSI_HyperVolume_s
	PSI_Solid_s
	PSI_HyperSolid_s
	///////////////////////// FIGURES
	PSI_Triangle_s
	PSI_Square_s
	PSI_Rectangle_s
	PSI_Polygon_s
	PSI_Pyramid_s
	PSI_Cube_s
	PSI_Sphere_s
	PSI_Polyhedron_s
	PSI_HyperPyramid_s
	PSI_HyperCube_s
	PSI_HyperSphere_s
	PSI_HyperPolyhedron_s
ENUMS:

TYPES:
	///////////////////////// ELEMENTS
	PSI_Point2D_t
	PSI_GL_Point2D_t
	PSI_Point3D_t
	PSI_GL_Point3D_t
	PSI_Point4D_t
	PSI_GL_Point4D_t
	PSI_PointnD_t
	PSI_GL_PointnD_t
	PSI_Line2D_t
	PSI_Line3D_t
	PSI_Line4D_t
	PSI_LineND_t
	PSI_Curve2D_t
	PSI_Curve3D_t
	PSI_Curve4D_t
	PSI_CurveND_t
	PSI_Angle_t
	PSI_ArcAngle_t
	PSI_SolidAngle_t
	PSI_Plane_t
	PSI_Surface_t
	PSI_Volume_t
	PSI_HyperVolume_t
	PSI_Solid_t
	PSI_HyperSolid_t
	///////////////////////// FIGURES
	PSI_Triangle_t
	PSI_GL_Triangle_t
	PSI_DX_Triangle_t
	PSI_Square_t
	PSI_GL_Square_t
	PSI_DX_Square_t
	PSI_Rectangle_t
	PSI_GL_Rectangle_t
	PSI_DX_Rectangle_t
	PSI_Polygon_t
	PSI_GL_Polygon_t
	PSI_DX_Polygon_t
	PSI_Pyramid_t
	PSI_GL_Pyramid_t
	PSI_DX_Pyramid_t
	PSI_Cube_t
	PSI_GL_Cube_t
	PSI_DX_Cube_t
	PSI_Sphere_t
	PSI_GL_Sphere_t
	PSI_DX_Sphere_t
	PSI_Polyhedron_t
	PSI_GL_Polyhedron_t
	PSI_DX_Polyhedron_t
	PSI_HyperPyramid_t
	PSI_GL_HyperPyramid_t
	PSI_DX_HyperPyramid_t
	PSI_HyperCube_t
	PSI_GL_HyperCube_t
	PSI_DX_HyperCube_t
	PSI_HyperSphere_t
	PSI_GL_HyperSphere_t
	PSI_DX_HyperSphere_t
	PSI_HyperPolyhedron_t
	PSI_GL_HyperPolyhedron_t
	PSI_DX_HyperPolyhedron_t
CLASSES:
	PSI_ITope
********************************************* */ 

#ifndef PSI_TOPES
#define PSI_TOPES

// POINT2D
typedef struct PSI_Point2D_s{
	int x;
	int y;
} PSI_Point2D_t;

typedef struct PSI_PointSeries2D_s{
	int size;
	PSI_Point2D_t* pointSeries;
} PSI_PointSeries2D_t;

typedef struct PSI_PointSeriesGL2D_s{
	int* index;
	int* vertex;
} PSI_PointSeriesGL2D_t;

template<typename T>
struct PSI_PointT2D_s{
	T x;
	T y;
};

template<typename T>
struct PSI_PointSeriesT2D_s{
	int size;
	PSI_PointT2D_s<T>* pointSeries;
};

// POINT3D
typedef struct PSI_Point3D_s{
	int x;
	int y;
	int z;
} PSI_Point3D_t;

typedef struct PSI_PointSeries3D_s{
	int size;
	PSI_Point3D_t* pointSeries;
} PSI_PointSeries3D_t;


template<typename T>
struct PSI_PointT3D_s{
	T x;
	T y;
	T z;
};

template<typename T>
struct PSI_PointSeriesT3D_s{
	int size;
	PSI_PointT3D_s* pointSeries;
};

// POINT4D
typedef struct PSI_Point4D_s{
	int x;
	int y;
	int z;
	int w;
} PSI_Point4D_t;

typedef struct PSI_PointSeries4D_s{
	int size;
	PSI_Point4D_t* pointSeries;
} PSI_PointSeries4D_t;

template<typename T>
struct PSI_PointT4D_s{
	T x;
	T y;
	T z;
	T w;
};

template<typename T>
struct PSI_PointSeriesT4D_s{
	int size;
	PSI_PointT4D_s* pointSeries;
};

// POINTND
typedef struct PSI_PointND_s{
	int size;
	int* p;
} PSI_PointND_t;

typedef struct PSI_PointSeriesND_s{
	int size;
	PSI_PointND_t* pointSeries;
} PSI_PointSeriesND_t;

template<typename T>
struct PSI_PointTND_s{
	int size;
	T* x;
};

template<typename T>
struct PSI_PointSerieTND_s{
	int size;
	PSI_PointND_s* pointSerie;
};

// LINE2D
typedef struct PSI_Line2D_s{
	PSI_Point2D_s start;
	PSI_Point2D_s end;
} PSI_Line2D_s;

// LINE3D
typedef struct PSI_Line3D_s{
	PSI_Point3D_s start;
	PSI_Point3D_s end;
} PSI_Line3D_s;

// LINE4D
typedef struct PSI_Line4D_s{
	PSI_Point4D_s start;
	PSI_Point4D_s end;
} PSI_Line4D_s;

// LINEND
typedef struct PSI_LineND_s{
	PSI_PointND_s start;
	PSI_PointND_s end;
} PSI_LineND_s;

// ANGLE
typedef int PSI_Angle_t;

// ARCANGLE
typedef struct PSI_arcAngle_s{
	PSI_Angle_t theta;
	PSI_Angle_t phi;
} PSI_arcAngle_t;

// SOLIDANGLE
typedef struct PSI_solidAngle_s{
	PSI_Angle_t theta;
	PSI_Angle_t phi;
	PSI_Angle_t rho;
} PSI_solidAngle_t;

// PLANE
typedef struct PSI_Plane_s{
	PSI_Point2D_t planePoint;
	PSI_Point2D_t normal;
} PSI_Plane_t;

// SURFACE
typedef struct PSI_Surface_s{
	PSI_Plane_t planes;
	PSI_Point2D_s** vertex;
} PSI_Surface_t;

// VOLUME
typedef struct PSI_Volume_s{
	PSI_Plane_t planes;
	PSI_Point2D_s** vertex;
} PSI_Volume_t;

typedef struct PSI_Volume_s{
	PSI_Plane_t planes;
	PSI_Point2D_s** vertex;
} PSI_Volume_t;

// SOLID
typedef struct PSI_Volume_s{
	PSI_Plane_t planes;
	PSI_Point2D_s** vertex;
} PSI_Volume_t;

typedef struct PSI_Volume_s{
	PSI_Plane_t planes;
	PSI_Point2D_s** vertex;
} PSI_Volume_t;

namespace PSI_MathUtility
{
    namespace PSI_GeometryToolbox
    {
        namespace PSI_TopesModule
        {
static class PSI_Topes: public virtual PSI_MathUtilityModule{
    public:
    PSI_Topes();
    ~PSI_Topes();
    private:
    protected:
};
class PSI_ITope{
	public:
		// CONSTRUCTORS
		PSI_ITope(){};
		~PSI_ITope(){};
		// PROPERTIES

		// METHODS
		virtual void magnitude() = 0;
		virtual void traslation() = 0;
		virtual void rotation() = 0;
		// OPERATORS
	private:
	protected:
};
        }
    }
} 


#endif