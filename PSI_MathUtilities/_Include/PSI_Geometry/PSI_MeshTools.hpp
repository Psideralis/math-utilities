/* *********************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_MeshTools.hpp
Description: 
********************************************* */ 

/* *********************************************
DEFINES:
    PSI_MESHTOOLS
MACROS:

STRUCTS:

ENUMS:

TYPES:

CLASSES:

********************************************* */ 

#ifndef PSI_MESHTOOLS
#define PSI_MESHTOOLS

namespace PSI_MathUtility
{
    namespace PSI_GeometryToolbox
    {
        namespace PSI_MeshModule
        {
static class PSI_Mesh: public virtual PSI_MathUtilityModule{
    public:
    PSI_Mesh();
    ~PSI_Mesh();
    private:
    protected:
};
        }
    }
} 

#endif