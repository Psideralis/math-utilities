/* ***************************************************************************************************************************************
Author: Psideralis
License: GNU GPL 3.0
File name: PSI_Geomtry Toolbox.hpp
Description: Geometry toolbox library includes:

    -Cartesian, Cylindrical, Spherical, Postcartesian coordinates (Generalized decoordinated matrix)
    -Point, Line, Curve, Plane, Surface, Volume, Solid, Hypersolid: 2D,3D,4D,ND
    -Angle, Arcangle, SolidAngle, HyperAngle: 2D,3D,4D,ND
    -Polytopes (Polyhedron, Polygon)
    -Manifolds
    -Reimannian Phase & Geometry (Curved Space: Normal forms)
        - Polytopes
        - Manifolds
    -Postreimannian Phase & Geometry (Curved Space: Nonclassic forms)
        - Polytopes
        - Manifolds
    -Geometrical algebra
        - Traslation
        - Rotation
        - Deformation
        - Fraction
    -Geometrical analysis
        - Curvature
        - Time dependant
    -Geometrical combinatorics
    -Mesh tools
        -Mesh analyzer: photo (image), video -> Mesh model: .obj, .stl, .dae, .glft, .glb
        -Mesh synthetizer

*************************************************************************************************************************************** */ 

#ifndef PSI_GEOMETRY
#define PSI_GEOMETRY

#ifndef CSTD
#define CSTD
	#include <iostream>
	#include <string>
	#include <fstream>
	#include <chrono>
	using namespace std;
	using namespace chrono;
#endif


namespace PSI_MathUtility
{
    namespace PSI_GeometryToolbox
    {
        static class PSI_Geometry: public virtual PSI_MathUtilityToolbox{

        }; 
    }
} 
#include "PSI_CoordinatesModule.hpp"
#include "PSI_ManifoldModule.hpp"
#include "PSI_TopesModule.hpp"

#endif